module bitbucket.org/grayll/grayll.io-grz-arkady

go 1.13

//replace bitbucket.org/grayll/grayll.io-utils => /home/bc/go/src/bitbucket.org/grayll/grayll.io-utils
replace bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c => /home/bc/go/src/bitbucket.org/huykbc/goautoneg

require (
	cloud.google.com/go v0.65.0
	cloud.google.com/go/firestore v1.3.0
	cloud.google.com/go/pubsub v1.3.1
	firebase.google.com/go v3.10.0+incompatible
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible // indirect
	github.com/SherClockHolmes/webpush-go v1.1.0
	github.com/algolia/algoliasearch-client-go/v3 v3.5.2
	github.com/antigloss/go v0.0.0-20200109080012-05d5d0918164
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/fatih/structs v1.0.0

	github.com/gin-gonic/gin v1.6.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1 // indirect
	github.com/huyntsgs/cors v1.3.2-0.20200526010755-d0b894e7ee83
	github.com/huyntsgs/hermes v0.0.0-20191119075450-4a9f1c2a64a0
	github.com/huyntsgs/stellar-service v0.0.0-20200511152020-7a130845cf0d

	github.com/imdario/mergo v0.3.8 // indirect
	github.com/jaytaylor/html2text v0.0.0-20190408195923-01ec452cbe43 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20150613213606-2caf8efc9366
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/olekukonko/tablewriter v0.0.2 // indirect
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/stellar/go v0.0.0-20190920224012-d72ea298f1e9
	github.com/ulule/limiter/v3 v3.5.0
	go.uber.org/goleak v0.10.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/api v0.30.0
	google.golang.org/genproto v0.0.0-20200825200019-8632dd797987
	google.golang.org/grpc v1.32.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
