package main

import (
	"fmt"
	"log"
	"time"
	//"bitbucket.org/grayll/grayll.io-grz-arkady/api"
)

func GenOpenPositionGRZ(position AlgorithmPosition) (string, string, string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Opened`, position.AlgorithmType)

	contents := []string{
		time.Unix(position.OpenPositionTimestamp, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`You have opened a new %s algorithmic position.`, GetAlgorithmString(position.AlgorithmType)),

		fmt.Sprintf(`Open Algo Position Value: $%.5f`, position.OpenPositionValueUSD),

		fmt.Sprintf(`%s Rate | $%.5f`, position.AlgorithmType[:3], position.OpenValueGRZ),

		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | $%.5f`, position.AlgorithmType, position.OpenPositionFeeUSD),

		fmt.Sprintf(`GRX Rate | $%.5f`, position.OpenValueGRX),

		fmt.Sprintf(`GRX | Total Algo Position Value | %.7f GRX`, position.OpenPositionTotalGRX),

		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | %.7f GRX`, position.AlgorithmType, position.OpenPositionFeeGRX),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s`, position.GrayllTxId),
	}
	content := ""
	for i, sent := range contents {
		if i > 0 && i != 2 {
			content = content + ". " + sent
		} else {
			content = content + sent
		}
	}

	url := "Stellar | Transaction ID | https://stellar.expert/explorer/public/search?term=" + position.StellarTxId
	log.Println(content)
	contents = append(contents, url)

	return title, content, url, contents
}

func GenPerformanceChange(positionMap map[string]interface{}, durationString, algorithm string) (string, string, string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Performance`, algorithm)
	openTime := int64(positionMap["open_position_timestamp"].(float64))
	last12hROIPercent := positionMap["current_position_ROI_percent"].(float64)
	if val, ok := positionMap["last_12h_ROI_percent"]; ok {
		last12hROIPercent = val.(float64)
	}
	contents := []string{
		time.Unix(openTime, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`Your scheduled %s algorithmic position performance notification for: `, algorithm),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s`, positionMap["grayll_transaction_id"].(string)),

		fmt.Sprintf(`%s algorithmic position | 24 hour ROI: %.5f%%`, algorithm, last12hROIPercent),

		fmt.Sprintf(`Algo Position Duration: %v`, durationString),

		fmt.Sprintf(`Current Algo Position Value: $%.5f`, positionMap["current_position_value_$"].(float64)),

		fmt.Sprintf(`Current GRX Rate | $%.7f`, positionMap["current_value_GRX"].(float64)),

		fmt.Sprintf(`Current Algo Position Value: %.7f GRX`, positionMap["current_position_value_GRX"].(float64)),

		fmt.Sprintf(`Total %s algorithmic position ROI: %.5f%%`, algorithm, positionMap["current_position_ROI_percent"].(float64)),
	}
	content := ""
	for i, sent := range contents {
		if i > 0 {
			content = content + ". " + sent
		} else {
			content = content + sent
		}
	}
	return title, content, "", contents
}

func GenClosePositionGRZ(position AlgorithmPosition) (string, string, string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Closed`, position.AlgorithmType)

	contents := []string{
		time.Unix(position.ClosePositionTimestamp, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`You have closed a %s position`, GetAlgorithmString(position.AlgorithmType)),
		fmt.Sprintf(`Algo Position Duration: %s`, position.DurationString),

		fmt.Sprintf(`Close Algo Position Value: $%.5f`, position.ClosePositionValueUSD),
		fmt.Sprintf(`%s Rate | $%.7f`, position.AlgorithmType[:3], position.CloseValueGRZ),
		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | $%.5f`, position.AlgorithmType, position.ClosePositionFeeUSD),
		fmt.Sprintf(`%s | Algo Performance Fee @ 18%% | $%.5f`, position.AlgorithmType, position.ClosePerformanceFeeUSD),

		fmt.Sprintf(`GRX Rate | $%.5f`, position.CloseValueGRX),
		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | %.7f GRX`, position.AlgorithmType, position.ClosePositionFeeGRX),
		fmt.Sprintf(`%s | Algo Performance Fee @ 18%% | %.7f GRX`, position.AlgorithmType, position.ClosePerformanceFeeGRX),

		fmt.Sprintf(`GRX | Close Algo Position Value | %.7f GRX`, position.ClosePositionValueGRX),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s`, position.GrayllTxId),
	}

	content := ""
	for i, sent := range contents {
		if i > 0 {
			content = content + ". " + sent
		} else {
			content = content + sent
		}
	}

	url := "Stellar | Transaction ID | https://stellar.expert/explorer/public/search?term=" + position.CloseStellarTxId
	contents = append(contents, url)
	return title, content, url, contents
}

func GenCloseAllPositionGRZ(position AlgorithmCloseAllPosition) (string, string, string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Closed`, position.AlgorithmType)
	contents := []string{
		time.Unix(position.ClosePositionTimestamp, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`You have closed %s position. Algo Position Duration: %s `, GetAlgorithmString(position.AlgorithmType), position.DurationString),

		fmt.Sprintf(`Close Algo Position Value: $%.5f`, position.ClosePositionValueUSD),
		fmt.Sprintf(`%s Rate | $%.7f`, position.AlgorithmType[:3], position.CloseValueGRZ),
		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | $%.5f`, position.AlgorithmType, position.ClosePositionFeeUSD),
		fmt.Sprintf(`%s | Algo Performance Fee @ 18%% | $%.5f`, position.AlgorithmType, position.ClosePerformanceFeeUSD),

		fmt.Sprintf(`GRX Rate | $%.5f`, position.CloseValueGRX),
		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | %.7f GRX`, position.AlgorithmType, position.ClosePositionFeeUSD),
		fmt.Sprintf(`%s | Algo Performance Fee @ 18%% | %.7f GRX`, position.AlgorithmType, position.ClosePerformanceFeeUSD),

		fmt.Sprintf(`GRX | Close Algo Position Value | %.7f GRX`, position.ClosePositionValueGRX),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s. `, position.GrayllTxId),
	}

	content := ""
	for i, sent := range contents {
		if i > 0 {
			content = content + ". " + sent
		} else {
			content = content + sent
		}
	}

	url := "Stellar | Transaction ID | https://stellar.expert/explorer/public/search?term=" + position.CloseStellarTxId
	//log.Println(content)
	contents = append(contents, url)

	return title, content, url, contents
}
