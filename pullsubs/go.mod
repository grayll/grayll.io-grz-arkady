module bitbucket.org/grayll/grayll.io-grz-arkady/pullsubs

go 1.12

require (
	bitbucket.org/grayll/grayll-system v0.0.0-20200629014437-e3731493bd51
	cloud.google.com/go/firestore v1.1.1
	cloud.google.com/go/pubsub v1.2.0
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible // indirect
	github.com/algolia/algoliasearch-client-go/v3 v3.8.0
	github.com/antigloss/go v0.0.0-20200109080012-05d5d0918164
	github.com/gin-contrib/cors v1.3.0 // indirect
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/google/uuid v1.1.1 // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/huyntsgs/hermes v1.2.0
	github.com/huyntsgs/stellar-service v0.0.0-20200209144523-337366ca82af
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/sendgrid/rest v2.6.0+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	github.com/stellar/go v0.0.0-20190920224012-d72ea298f1e9
	google.golang.org/api v0.15.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/russross/blackfriday.v2 v2.0.1 // indirect

)

replace gopkg.in/russross/blackfriday.v2 v2.0.1 => github.com/russross/blackfriday/v2 v2.0.1

replace bitbucket.org/grayll/grayll-system v0.0.0-20200629014437-e3731493bd51 => /home/bc/go/src/bitbucket.org/grayll/grayll-system
