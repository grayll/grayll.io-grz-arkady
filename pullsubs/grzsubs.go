package main

import (
	"flag"
	"log"
	"strings"
	"sync"

	//"strconv"

	//"net/http"
	"os"
	"os/signal"
	"time"

	"context"
	"fmt"

	//"io"
	//"errors"
	"cloud.google.com/go/firestore"
	// firebase "firebase.google.com/go"
	"runtime"

	"github.com/huyntsgs/stellar-service/assets"

	"google.golang.org/api/option"

	"cloud.google.com/go/pubsub"
	//"github.com/GeertJohan/go.rice"
	stellar "github.com/huyntsgs/stellar-service"

	utilities "bitbucket.org/grayll/grayll-system"
	"github.com/algolia/algoliasearch-client-go/v3/algolia/search"
	"gopkg.in/natefinch/lumberjack.v2"
)

type AppContext struct {
	Client               *firestore.Client
	GrayllAppClient      *firestore.Client
	GrzClient            *firestore.Client
	PubsubClient         *pubsub.Client
	Cache                *utilities.RedisCache
	Config               *Config
	AlgoliaIndex         *search.Index
	AlgoliaReferralIndex *search.Index
	Asset                assets.Asset
	NoWorker             int
	ChanBuf              int
	IsReconnectFs        bool
	IsReconnectFsApp     bool
	IsReconnectRedis     bool
	mu                   sync.Mutex
	muApp                sync.Mutex
	muRedis              sync.Mutex
}
type PriceH struct {
	Price  float64 `json:"price"`
	UnixTs int64   `json:"unixts"`
	GRType string  `json:"grtype"`
	Frame  string  `json:"frame"`
}
type ErrorReport struct {
	LogType  string
	ReportAt int64
	Period   int64
}

var (
	errReport ErrorReport = ErrorReport{LogType: "FireStore", ReportAt: 0, Period: 4 * 60 * 60}
)

// For pull subscription
func main() {

	ctx, cancel := context.WithCancel(context.Background())
	var err error
	projectID := os.Getenv("GOOGLE_CLOUD_PROJECT")
	if projectID == "" {
		projectID = "grayll-system"
		log.Println("GOOGLE_CLOUD_PROJECT is not set. Will use default: ", projectID)
	}
	isProd := flag.Bool("prod", false, "run on prod or local")
	openClose := flag.Bool("openclose", false, "open close subscription")

	noWorker := flag.Int("noworker", 1, "number of worker goroutine")
	chanbuf := flag.Int("chanbuf", 5, "number of buffer channel")

	flag.Parse()
	configPath := ""
	appContext := new(AppContext)
	appContext.NoWorker = *noWorker
	appContext.ChanBuf = *chanbuf
	if *isProd {
		configPath = "/home/development/"
		appContext.Config = parseConfig(configPath + "config1.json")

	} else {
		configPath = "/home/bc/go/src/bitbucket.org/grayll/grayll.io-grz-arkady/pullsubs/config/"
		appContext.Config = parseConfig(configPath + "config.json")
	}

	log.SetOutput(&lumberjack.Logger{
		Filename:   configPath + "log/pull-log.txt",
		MaxSize:    2, // megabytes
		MaxBackups: 2,
		MaxAge:     10,   //days
		Compress:   true, // disabled by default
	})

	log.Println("CONFIG-isMainnet:", appContext.Config.IsMainNet)

	stellar.SetupParams(float64(1000), appContext.Config.IsMainNet)

	algoliaClient := search.NewClient("BXFJWGU0RM", "ef746e2d654d89f2a32f82fd9ffebf9e")
	appContext.AlgoliaIndex = algoliaClient.InitIndex("algoPosition-ua")
	appContext.AlgoliaReferralIndex = algoliaClient.InitIndex("referrals-tx")

	if appContext.Client == nil {
		if *isProd {
			time.Sleep(30 * time.Second)
			ttl, _ := time.ParseDuration("12h")
			appContext.Cache, err = utilities.NewRedisCache(ttl, appContext.Config.RedisHost, appContext.Config.RedisPass, appContext.Config.RedisPort)

			if err != nil {
				log.Fatalln("ERROR main - unable connect redis", err)
			}
			appContext.PubsubClient, err = pubsub.NewClient(ctx, projectID)
			if err != nil {
				log.Println("pubsub.NewClient error:", err)
				return
			}
			appContext.ReConnectFireStore("", 0)

		} else {
			ttl, _ := time.ParseDuration("12h")
			appContext.Cache, err = utilities.NewRedisCache(ttl, appContext.Config.RedisHost, appContext.Config.RedisPass, appContext.Config.RedisPort)
			opt := option.WithCredentialsFile("../grayll-system-b2317e64062b.json")
			appContext.Client, err = firestore.NewClient(ctx, projectID, opt)
			if err != nil {
				log.Println("firestore.NewClient error: ", err)
				return
			}
			appContext.PubsubClient, err = pubsub.NewClient(ctx, projectID, opt)
			if err != nil {
				log.Println("pubsub.NewClient error:", err)
				return
			}

			opt = option.WithCredentialsFile("./grayll-app-f3f3f3-firebase-adminsdk-vhclm-e074da6170.json")
			appContext.GrayllAppClient, err = firestore.NewClient(ctx, "grayll-app-f3f3f3", opt)
			if err != nil {
				log.Println("firestore.NewClient error: ", err)
				return
			}
			opt = option.WithCredentialsFile("./grayll-grz-arkady-firebase-adminsdk-9q3s2-3198dd40fa.json")
			appContext.GrzClient, err = firestore.NewClient(ctx, "grayll-grz-arkady", opt)
			if err != nil {
				log.Println("firestore.NewClient error: ", err)
				return
			}
		}
	}

	// go func() {
	// 	pullMsgsConcurrenyControl(ctx, projectID, "pull_mvp_price")
	// }()

	asset := assets.Asset{Code: appContext.Config.AssetCode, IssuerAddress: appContext.Config.IssuerAddress}
	appContext.Asset = asset

	subId := appContext.Config.PositionSubs
	if *openClose {
		subId = appContext.Config.PositionSubsOpenClose
	}
	log.Println("subId:", subId)

	go func() {
		appContext.pullMsgsConcurrenyControl(ctx, projectID, subId)
	}()

	// Wait for SIGINT.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	<-sig

	// Shutdown. Cancel application context will kill all attached tasks.
	cancel()

}

func (appContext *AppContext) CheckClientConnection() error {
	CheckConnectionPath := "check_connection/p46Pz3LP5C2HNyD3LVEX"
	ctx := context.Background()
	// Check firestore connection
	_, err := appContext.Client.Doc(CheckConnectionPath).Get(ctx)
	if err != nil {
		log.Println("[ERROR] check grayll-system connection: ", CheckConnectionPath, err)
		appContext.ReconnectFireStoreSys()
		_, err := appContext.Client.Doc(CheckConnectionPath).Get(ctx)
		if err != nil {
			log.Println("[ERROR] check grayll-system connection retry: ", CheckConnectionPath, err)
			return err
		}
	}
	_, err = appContext.GrayllAppClient.Doc(CheckConnectionPath).Get(ctx)
	if err != nil {
		log.Println("[ERROR] Check grayll-app connection error: ", err)
		appContext.ReconnectFireStoreApp()
		_, err = appContext.GrayllAppClient.Doc(CheckConnectionPath).Get(ctx)
		if err != nil {
			log.Println("[ERROR] Check grayll-app connection retry error: ", err)
			return err
		}
	}
	return err
}

func (appContext *AppContext) ReConnectFireStore(errMsg string, id int) error {

	ctx := context.Background()
	var err error
	if strings.Contains(errMsg, "closing") || errMsg == "" {
		if (errReport.ReportAt < time.Now().Unix()-errReport.Period || errReport.ReportAt == 0) && errMsg != "" {
			errReport.ReportAt = time.Now().Unix()
			SendNoticeMail("huykbc@gmail.com", "Huy", "ERROR Firestore conection", []string{err.Error()})
		}
		cnt := 0
		for {
			cnt++
			time.Sleep(1 * time.Second)
			if id == 0 {
				appContext.Client, err = firestore.NewClient(ctx, "grayll-system")
				if err != nil {
					log.Fatalln("firestore.NewClient error: ", err)
					return err
				}
				appContext.GrayllAppClient, err = firestore.NewClient(ctx, "grayll-app-f3f3f3")
				if err != nil {
					log.Fatalln("firestore.NewClient error: ", err)
					return err
				}
				appContext.GrzClient, err = firestore.NewClient(ctx, "grayll-grz-arkady")
				if err != nil {
					log.Println("firestore.NewClient error: ", err)
					return err
				}
			} else {
				switch id {
				case 1:
					appContext.Client, err = firestore.NewClient(ctx, "grayll-system")
					if err != nil {
						log.Fatalln("firestore.NewClient error: ", err)
						return err
					}
				case 2:
					appContext.GrayllAppClient, err = firestore.NewClient(ctx, "grayll-app-f3f3f3")
					if err != nil {
						log.Fatalln("firestore.NewClient error: ", err)
						return err
					}
				case 3:
					appContext.GrzClient, err = firestore.NewClient(ctx, "grayll-grz-arkady")
					if err != nil {
						log.Println("firestore.NewClient error: ", err)
						return err
					}
				}
			}
			if err == nil {
				break
			}
			if cnt > 120 {
				log.Fatalln("[ERROR] Can not connect to Firestore", err)
			}

		}

	}
	return err

}

func (appContext *AppContext) pullMsgsConcurrenyControl(ctx context.Context, projectID, subId string) error {
	if appContext.PubsubClient == nil {
		log.Println("appContext is nil")
	}
	sub := appContext.PubsubClient.Subscription(subId)

	// Must set ReceiveSettings.Synchronous to false (or leave as default) to enable
	// concurrency settings. Otherwise, NumGoroutines will be set to 1.
	sub.ReceiveSettings.Synchronous = false
	// NumGoroutines is the number of goroutines sub.Receive will spawn to pull messages concurrently.
	sub.ReceiveSettings.NumGoroutines = runtime.NumCPU()

	// Receive messages for 10 seconds.
	//ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// cm := make(chan *pubsub.Message)
	// // Handle individual messages in a goroutine.

	// go func() {
	// 	for {
	// 		select {
	// 		case msg := <-cm:
	// 			go func() {
	// 				ProcessAlgoData(ctx, appContext, msg)
	// 			}()
	// 			msg.Ack()
	// 		case <-ctx.Done():
	// 			return
	// 		}
	// 	}
	// }()

	cm := make(chan *pubsub.Message, appContext.ChanBuf)
	// Handle individual messages in a goroutine.

	for i := 0; i < appContext.NoWorker; i++ {
		go func() {
			for {
				select {
				case msg := <-cm:
					go func() {
						ProcessAlgoData(ctx, appContext, msg)
					}()
					//msg.Ack()
				case <-ctx.Done():
					return
				}
			}
		}()
	}

	// Receive blocks until the context is cancelled or an error occurs.
	err := sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		cm <- msg
	})
	if err != nil {
		return fmt.Errorf("Receive: %v", err)
	}
	close(cm)

	return nil
}

func PublishMessage(topicID string, appContext *AppContext, msg []byte) error {

	ctx := context.Background()
	t := appContext.PubsubClient.Topic(topicID)
	result := t.Publish(ctx, &pubsub.Message{Data: msg})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	id, err := result.Get(ctx)
	if err != nil {
		log.Printf("Published error: %v\n", err)
		return fmt.Errorf("Get: %v", err)
	}
	log.Printf("Published message ID: %v\n", id)
	return nil
}

func (apiContext *AppContext) ReconnectFireStoreSys() {

	if !apiContext.IsReconnectFs {
		apiContext.SetReconnectFs(true)
		apiContext.Client, _ = ReconnectFireStore("grayll-system", 60)
		apiContext.SetReconnectFs(false)
		return
	} else {
		cnt := 0
		for {
			cnt++
			time.Sleep(time.Second * 1)
			if !apiContext.IsReconnectFs {
				break
			}
			if cnt > 60 {
				break
			}
		}
		return
	}

}
func (apiContext *AppContext) ReconnectFireStoreApp() {

	if !apiContext.IsReconnectFsApp {
		apiContext.SetReconnectFsApp(true)
		apiContext.Client, _ = ReconnectFireStore("grayll-app-f3f3f3", 60)
		apiContext.SetReconnectFsApp(false)
		return
	} else {
		cnt := 0
		for {
			cnt++
			time.Sleep(time.Second * 1)
			if !apiContext.IsReconnectFs {
				break
			}
			if cnt > 60 {
				break
			}
		}
		return
	}

}
func (apiContext *AppContext) ReconnectFireStoreGryz() {

	if !apiContext.IsReconnectFs {
		apiContext.SetReconnectFs(true)
		apiContext.GrzClient, _ = ReconnectFireStore("grayll-grz-arkady", 60)
		apiContext.SetReconnectFs(false)
	} else {
		cnt := 0
		for {
			cnt++
			time.Sleep(time.Second * 1)
			if !apiContext.IsReconnectFs {
				break
			}
			if cnt > 60 {
				break
			}
		}
		return
	}

}
func (apiContext *AppContext) ReconnectRedis() {

	if !apiContext.IsReconnectRedis {
		apiContext.SetReconnectRedis(true)
		ttl, _ := time.ParseDuration("12h")
		apiContext.Cache, _ = utilities.NewRedisCache(ttl, apiContext.Config.RedisHost, apiContext.Config.RedisPass, apiContext.Config.RedisPort)
		apiContext.SetReconnectRedis(false)
		return
	} else {
		cnt := 0
		for {
			cnt++
			time.Sleep(time.Second * 1)
			if !apiContext.IsReconnectRedis {
				break
			}
			if cnt > 60 {
				break
			}
		}
		return
	}

}

func (apiContext *AppContext) SetReconnectFs(status bool) {
	apiContext.mu.Lock()
	defer apiContext.mu.Unlock()
	apiContext.IsReconnectFs = status
}
func (apiContext *AppContext) SetReconnectFsApp(status bool) {
	apiContext.muApp.Lock()
	defer apiContext.muApp.Unlock()
	apiContext.IsReconnectFsApp = status
}
func (apiContext *AppContext) SetReconnectRedis(status bool) {
	apiContext.muRedis.Lock()
	defer apiContext.muRedis.Unlock()
	apiContext.IsReconnectRedis = status
}
