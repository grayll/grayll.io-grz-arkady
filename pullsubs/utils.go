package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/huyntsgs/stellar-service/assets"
	"github.com/stellar/go/clients/horizonclient"
	"github.com/stellar/go/protocols/horizon/operations"
)

type (
	Config struct {
		HotWalletOne     string `json:"hotWalletOne"`
		HotWalletOneSeed string `json:"hotWalletOneSeed"`
		HotWalletTwo     string `json:"hotWalletTwo"`
		HotWalletTwoSeed string `json:"hotWalletTwoSeed"`
		IsMainNet        bool   `json:"isMainNet"`
		AssetCode        string `json:"assetCode"`
		IssuerAddress    string `json:"issuerAddress"`
		XlmLoanerSeed    string `json:"xlmLoanerSeed"`
		XlmLoanerAddress string `json:"xlmLoanerAddress"`

		RedisHost             string  `json:"redisHost"`
		RedisPort             int     `json:"redisPort"`
		RedisPass             string  `json:"redisPass"`
		HorizonUrl            string  `json:"horizonUrl"`
		Host                  string  `json:"host"`
		Numberify             string  `json:"numberify"`
		SuperAdminAddress     string  `json:"superAdminAddress"`
		SuperAdminSeed        string  `json:"superAdminSeed"`
		SellingPrice          float64 `json:"sellingPrice"`
		SellingPercent        int     `json:"sellingPercent"`
		PositionSubs          string  `json:"positionSubs"`
		PositionSubsOpenClose string  `json:"positionSubsOpenClose"`
	}
	UserData struct {
		UserId        float64 `json:"user_id"`
		AlgorithmType string  `json:"algorithm_type"`
		Start         int     `json:"start"`
		Count         int     `json:"count"`
	}
	AlgoInputData struct {
		Action string            `json:"action"`
		Data   AlgorithmPosition `json:"data"`
	}
	AlgoCloseAllInputData struct {
		Action string                      `json:"action"`
		Data   []AlgorithmCloseAllPosition `json:"data"`
	}

	AlgorithmPosition struct {
		UserId               string  `json:"user_id"`
		OpenPositionTotalUSD float64 `json:"open_position_total_$,omitempty"`
		StellarTxId          string  `json:"open_stellar_transaction_id,omitempty"`

		GrayllTxId             string `json:"grayll_transaction_id,omitempty"`
		AlgorithmType          string `json:"algorithm_type,omitempty"`
		OpenPositionTimestamp  int64  `json:"open_position_timestamp,omitempty"`
		ClosePositionTimestamp int64  `json:"close_position_timestamp,omitempty"`

		Duration       int64  `json:"duration"`
		DurationString string `json:"duration_string"`
		Status         string `json:"status"`
		Action         string `json:"action"`

		CurrentValueGRX              float64 `json:"current_value_GRX,omitempty"`
		CurrentValueGRZ              float64 `json:"current_value_GRZ,omitempty"`
		CurrentValueGRY              float64 `json:"current_value_GRY,omitempty"`
		CurrentPositionValueUSD      float64 `json:"current_position_value_$,omitempty"`
		CurrentPositionValueGRX      float64 `json:"current_position_value_GRX,omitempty"`
		CurrentPositionROIUSD        float64 `json:"current_position_ROI_$"`
		CurrentPositionROIPercentage float64 `json:"current_position_ROI_percent"`

		OpenValueGRX         float64 `json:"open_value_GRX,omitempty"`
		OpenValueGRZ         float64 `json:"open_value_GRZ,omitempty"`
		OpenValueGRY         float64 `json:"open_value_GRY,omitempty"`
		OpenPositionFeeUSD   float64 `json:"open_position_fee_$,omitempty"`
		OpenPositionFeeGRX   float64 `json:"open_position_fee_GRX,omitempty"`
		OpenPositionValueUSD float64 `json:"open_position_value_$,omitempty"`
		OpenPositionTotalGRX float64 `json:"open_position_total_GRX,omitempty"`
		OpenPositionValueGRZ float64 `json:"open_position_value_GRZ,omitempty"`
		OpenPositionValueGRX float64 `json:"open_position_value_GRX,omitempty"`

		CloseStellarTxId string `json:"close_stellar_transaction_id,omitempty"`

		CloseValueGRX         float64 `json:"close_value_GRX"`
		CloseValueGRZ         float64 `json:"close_value_GRZ"`
		ClosePositionTotalUSD float64 `json:"close_position_total_$"`
		ClosePositionValueUSD float64 `json:"close_position_value_$"`

		ClosePositionTotalGRX float64 `json:"close_position_total_GRX"`
		ClosePositionValueGRX float64 `json:"close_position_value_GRX"`
		ClosePositionTotalGRZ float64 `json:"close_position_total_GRZ"`
		//ClosePositionValueGRZ float64 `json:"close_position_value_GRZ"`

		ClosePositionROIUSD            float64 `json:"close_position_ROI_$"`
		ClosePositionROIPercentage     float64 `json:"close_position_ROI_percent"`
		Close_position_ROI_percent_NET float64 `json:"close_position_ROI_percent_NET"`
		ClosePositionFeeGRX            float64 `json:"close_position_fee_GRX"`
		ClosePositionFeeUSD            float64 `json:"close_position_fee_$"`
		ClosePerformanceFeeUSD         float64 `json:"close_performance_fee_$"`
		ClosePerformanceFeeGRX         float64 `json:"close_performance_fee_GRX"`
	}

	AlgorithmCloseAllPosition struct {
		UserId               string  `json:"user_id"`
		RefererId            string  `json:"referer_id,omitempty"`
		OpenPositionTotalUSD float64 `json:"open_position_total_$,omitempty"`
		StellarTxId          string  `json:"open_stellar_transaction_id,omitempty"`
		CloseStellarTxId     string  `json:"close_stellar_transaction_id,omitempty"`

		GrayllTxId             string `json:"grayll_transaction_id,omitempty"`
		AlgorithmType          string `json:"algorithm_type,omitempty"`
		OpenPositionTimestamp  int64  `json:"open_position_timestamp,omitempty"`
		ClosePositionTimestamp int64  `json:"close_position_timestamp,omitempty"`

		Duration       int64  `json:"duration"`
		DurationString string `json:"duration_string"`
		Status         string `json:"status"`
		Action         string `json:"action"`

		CurrentValueGRX              float64 `json:"current_value_GRX,omitempty"`
		CurrentValueGRZ              float64 `json:"current_value_GRZ,omitempty"`
		CurrentValueGRY              float64 `json:"current_value_GRY,omitempty"`
		CurrentPositionValueUSD      float64 `json:"current_position_value_$,omitempty"`
		CurrentPositionValueGRX      float64 `json:"current_position_value_GRX,omitempty"`
		CurrentPositionROIUSD        float64 `json:"current_position_ROI_$"`
		CurrentPositionROIPercentage float64 `json:"current_position_ROI_percent"`

		OpenValueGRX         float64 `json:"open_value_GRX,omitempty"`
		OpenValueGRZ         float64 `json:"open_value_GRZ,omitempty"`
		OpenValueGRY         float64 `json:"open_value_GRY,omitempty"`
		OpenPositionFeeUSD   float64 `json:"open_position_fee_$,omitempty"`
		OpenPositionFeeGRX   float64 `json:"open_position_fee_GRX,omitempty"`
		OpenPositionValueUSD float64 `json:"open_position_value_$,omitempty"`
		OpenPositionTotalGRX float64 `json:"open_position_total_GRX,omitempty"`
		OpenPositionValueGRZ float64 `json:"open_position_value_GRZ,omitempty"`
		OpenPositionValueGRX float64 `json:"open_position_value_GRX,omitempty"`

		CloseValueGRX         float64 `json:"close_value_GRX"`
		CloseValueGRZ         float64 `json:"close_value_GRZ"`
		ClosePositionTotalUSD float64 `json:"close_position_total_$"`
		ClosePositionValueUSD float64 `json:"close_position_value_$"`

		ClosePositionTotalGRX float64 `json:"close_position_total_GRX"`
		ClosePositionValueGRX float64 `json:"close_position_value_GRX"`
		ClosePositionTotalGRZ float64 `json:"close_position_total_GRZ"`
		//ClosePositionValueGRZ float64 `json:"close_position_value_GRZ"`

		ClosePositionROIUSD            float64 `json:"close_position_ROI_$"`
		ClosePositionROIPercentage     float64 `json:"close_position_ROI_percent"`
		Close_position_ROI_percent_NET float64 `json:"close_position_ROI_percent_NET"`
		ClosePositionFeeGRX            float64 `json:"close_position_fee_GRX"`
		ClosePositionFeeUSD            float64 `json:"close_position_fee_$"`
		ClosePerformanceFeeUSD         float64 `json:"close_performance_fee_$"`
		ClosePerformanceFeeGRX         float64 `json:"close_performance_fee_GRX"`
		ClosePerfFeeRefererGRX         float64 `json:"close_perf_fee_referer_GRX,omitempty"`
	}

	AlgorithmData struct {
		AlgorithmType   string
		PositionsClosed int64
		PositionRoi     float64
	}
)

func SendGrx(fullAmount float64, userAddress, hotWalletTwoSeed string, asset assets.Asset) (string, error) {
	amount := fmt.Sprintf("%.7f", fullAmount)
	amountn, _ := strconv.ParseFloat(amount, 64)
	_, closeTxId, err := assets.SendAsset(asset, userAddress, amountn, hotWalletTwoSeed, "")
	if err != nil {
		log.Println("Can send from hotwallet2 to user", userAddress, err)
		time.Sleep(time.Second * 10)
		_, closeTxId, err = assets.SendAsset(asset, userAddress, amountn, hotWalletTwoSeed, "")
		if err != nil {
			log.Println("Can send from hotwallet2 to user", userAddress, err)
			return "", err
		}
	}
	return closeTxId, err
}
func SendGrxMemo(fullAmount float64, userAddress, hotWalletTwoSeed string, asset assets.Asset, grayllTx string) (string, error) {
	amount := fmt.Sprintf("%.7f", fullAmount)
	amountn, _ := strconv.ParseFloat(amount, 64)
	shortTx := strings.TrimLeft(grayllTx, "0")
	_, closeTxId, err := assets.SendAsset(asset, userAddress, amountn, hotWalletTwoSeed, shortTx)
	if err != nil {
		log.Println("Can send from hotwallet2 to user", userAddress, err)
		time.Sleep(time.Second * 10)
		_, closeTxId, err = assets.SendAsset(asset, userAddress, amountn, hotWalletTwoSeed, shortTx)
		if err != nil {
			log.Println("Can send from hotwallet2 to user", userAddress, err)
			return "", err
		}
	}
	return closeTxId, err
}
func ConvertSectoDay(duration int64) string {
	day := duration / (24 * 3600)

	duration = duration % (24 * 3600)
	hour := duration / 3600

	duration %= 3600
	minutes := duration / 60

	duration %= 60
	//seconds := duration

	return fmt.Sprintf("%02d:%02d | %03d", hour, minutes, day)
}

func ParsePaymentFromTxHash(txHash string, client *horizonclient.Client) ([]operations.Payment, error) {
	opRequest := horizonclient.OperationRequest{ForTransaction: txHash}
	ops, err := client.Operations(opRequest)

	payments := make([]operations.Payment, 0)
	if err != nil {
		return payments, err
	}
	for _, record := range ops.Embedded.Records {

		payment, ok := record.(operations.Payment)
		if ok {
			payments = append(payments, payment)
		}

	}
	return payments, nil
}
