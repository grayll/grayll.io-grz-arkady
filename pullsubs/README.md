# Build & publish cloud run

sudo gcloud projects list

gcloud config set project grayll-system

gcloud pubsub topics create gryprice
gcloud pubsub topics create grzprice

gcloud pubsub subscriptions create mySubscription --topic myTopic --push-endpoint="https://myapp.appspot.com/push"

   gcloud pubsub subscriptions list
   gcloud pubsub topics list-subscriptions myTopic
   gcloud pubsub subscriptions delete mySubscription

gcloud builds submit --tag gcr.io/grayll-app-f3f3f3/pullsubscription

- Build docker image
gcloud beta run deploy --image gcr.io/grayll-app-f3f3f3/pullsubscription --platform managed

- Deploy and run
sudo gcloud beta run deploy pubsub-tutorial --image gcr.io/grayll-app-f3f3f3/pullsubscription

gcloud compute scp ./pullsubs pullsubscription:~

$ sudo systemctl daemon-reload
Start the service with:
sudo systemctl stop pullsubscription
sudo cp /home/bc/pullsubs ./pullsubscription
sudo cp /home/bc/config1.json .
systemctl daemon-reload
sudo systemctl start pullsubscription
sudo systemctl stop pullsubscription
sudo systemctl enable pullsubscription
sudo systemctl status pullsubscription

journalctl --since "2020-06-19 02:40:10"