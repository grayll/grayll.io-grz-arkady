package main

import (
	"encoding/json"
	"errors"

	"log"
	//"strconv"
	"strings"

	//"net/http"

	"time"

	// "github.com/gin-contrib/cors"
	// "github.com/gin-gonic/gin"

	"context"
	"fmt"

	//"io"
	//"errors"
	"cloud.google.com/go/firestore"
	// firebase "firebase.google.com/go"
	// "google.golang.org/api/iterator"

	"github.com/antigloss/go/logger"

	"cloud.google.com/go/pubsub"
	//"github.com/GeertJohan/go.rice"

	//stellar "github.com/huyntsgs/stellar-service"
	"bitbucket.org/grayll/grayll-system/mail"
	"github.com/algolia/algoliasearch-client-go/v3/algolia/opt"
	//"github.com/huyntsgs/stellar-service/assets"
)

const (
	TIME_24H_SECONDS = 86400
	TIME_24H_MINUTES = 1440
	TIME_7D_SECONDS  = 604800
	TIME_7D_MINUTES  = 10080

	// TIME_24H_SECONDS = 10800
	// TIME_24H_MINUTES = 180
	// TIME_7D_SECONDS  = 21600
	// TIME_7D_MINUTES  = 360
	CheckConnectionPath = "check_connection/p46Pz3LP5C2HNyD3LVEX"
)

func ProcessAlgoData(ctx context.Context, appContext *AppContext, msg *pubsub.Message) error {

	algoData := make(map[string]interface{})

	err := json.Unmarshal(msg.Data, &algoData)
	if err != nil {
		log.Println("json.Unmarshal error: ", err)
		return err
	}

	//action: open,close,update
	ok := false
	var action interface{}

	if action, ok = algoData["action"]; !ok {
		if action, ok = algoData["status"]; !ok {
			log.Println("Can not find action or status in data")
			return errors.New("Can not find action or status in data")
		}
	}

	if action.(string) == "OPEN" {
		user_id := algoData["user_id"].(string)
		grayll_tx_id := algoData["grayll_transaction_id"].(string)

		if user_id == "" {
			log.Println("Invalid user_id empty")
			return errors.New("invalid userid or grayll tx id")
		}

		// Check firestore connection
		err = appContext.CheckClientConnection()
		if err != nil {
			msg.Nack()
			log.Fatalln("[ERROR] Can not connect to firestore:", err)
		}

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", user_id, grayll_tx_id)
		_, err = appContext.Client.Doc(docPath).Set(ctx, algoData)
		if err != nil {
			log.Println("Save algo position data error: ", err)
			appContext.ReconnectFireStoreSys()
			_, err = appContext.Client.Doc(docPath).Set(ctx, algoData)
			if err != nil {
				msg.Nack()
				return err
			}
		}

		_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData)
		if err != nil {
			logger.Error("Save algo position data error: ", err)
			appContext.ReconnectFireStoreApp()
			_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData)
			if err != nil {
				msg.Nack()
				return err
			}
		}

		// Save notice for user and update unread message
		algorithm := algoData["algorithm_type"].(string)
		cacheTxId, unreadPath := GetCacheTxId(algorithm, grayll_tx_id)

		_, err = appContext.GrayllAppClient.Doc("users_meta/"+user_id).Update(ctx, []firestore.Update{
			{Path: unreadPath, Value: firestore.Increment(1)},
		})
		if err != nil {
			logger.Error("Unread update error: ", unreadPath, err, cacheTxId)
			//return
		}

		// Send mail and notice
		// push notice and mail
		position := AlgorithmPosition{}
		err := json.Unmarshal(msg.Data, &position)
		if err != nil {
			log.Println("json.Unmarshal error: ", err)

			return err
		}

		title, content, url, contents := GenOpenPositionGRZ(position)
		notice := map[string]interface{}{
			"title":       title,
			"body":        content,
			"type":        position.AlgorithmType,
			"isRead":      false,
			"time":        int64(algoData["open_position_timestamp"].(float64)),
			"url":         strings.Replace(url, "Stellar | Transaction ID | ", "", 1),
			"stellarTxId": position.StellarTxId,
		}
		docRef := appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
		//batch.Create(docRef, notice)
		_, err = docRef.Set(ctx, notice)

		//_, err = batch.Commit(ctx)
		if err != nil {
			appContext.ReconnectFireStoreApp()
			log.Println("ERROR OPEN - SaveNotice error: ", err)
			docRef = appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
			//batch.Create(docRef, notice)
			_, err = docRef.Set(ctx, notice)
			if err != nil {
				log.Println("ERROR OPEN - SaveNotice after retried:", err)
			}
		}

		// Update metric data in cache
		openPositions, err := appContext.Cache.UpdatePositionOpen(user_id, position.AlgorithmType, position.GrayllTxId, position.OpenPositionValueUSD)
		if err != nil {
			log.Println("[ERROR]-update position error", err)
		}
		_, totalCurrentValue := appContext.Cache.GetCurrentValues(user_id, position.AlgorithmType, false)
		log.Println("openPositions, totalCurrentValue", openPositions, totalCurrentValue)
		str := strings.ToLower(strings.ReplaceAll(position.AlgorithmType, " ", ""))
		key := fmt.Sprintf("total_%s_open_positions", str)
		key1 := fmt.Sprintf("total_%s_current_position_value_$", str)
		if totalCurrentValue > 0 && openPositions > 0 {
			appContext.GrayllAppClient.Doc("users_meta/"+user_id).Set(ctx, map[string]interface{}{
				key:  openPositions,
				key1: totalCurrentValue,
			}, firestore.MergeAll)
		}
		// End metric cache

		// Mail wallet
		go func() {
			mailAlgo, err := appContext.Cache.GetNotice(user_id, "MailAlgo")
			if err == nil {
				// check setting and send mail
				if mailAlgo == "1" {
					//log.Println("Start send mail notice")
					userRef, err := appContext.GrayllAppClient.Doc("users/" + user_id).Get(ctx)
					if userRef != nil {
						err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
						if err != nil {
							log.Println("SendNoticeMail error: ", err)
						}
					}
				}
			} else {
				log.Println("Can not get MailWallet setting from cache:", err)
			}
		}()
		msg.Ack()

	} else if action == "CLOSED" {
		//status := algoData["status"].(string)
		user_id := algoData["user_id"].(string)
		grayll_tx_id := algoData["grayll_transaction_id"].(string)

		if user_id == "" {
			log.Println("Invalid user_id empty")
			return errors.New("invalid userid or grayll tx id")
		}
		// Check firestore connection
		err = appContext.CheckClientConnection()
		if err != nil {
			msg.Nack()
			log.Fatalln("[ERROR] Can not connect to firestore:", err)
		}

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", user_id, grayll_tx_id)

		closeTime := time.Now().Unix()
		algoData["close_position_timestamp"] = closeTime
		if _, ok := algoData["open_position_timestamp"]; !ok {
			return nil
		}
		algoData["duration"] = closeTime - int64(algoData["open_position_timestamp"].(float64))

		userAddress, err := appContext.Cache.GetPublicKeyFromUid(algoData["user_id"].(string))
		if err != nil {
			log.Println("Can not get public key from uid", algoData["user_id"].(string), err)
			appContext.ReconnectRedis()
			userAddress, err = appContext.Cache.GetPublicKeyFromUid(algoData["user_id"].(string))
			if err != nil {
				log.Println("Can not get public key from uid", algoData["user_id"].(string), err)
				return err
			}
		}

		closeTxId, err := SendGrxMemo(algoData["close_position_value_GRX"].(float64), userAddress,
			appContext.Config.HotWalletTwoSeed, appContext.Asset, grayll_tx_id)

		if err == nil {
			msg.Ack()
		} else {
			msg.Nack()
			return err
		}

		log.Println("Send back user with ledger", userAddress, closeTxId)
		algoData["close_stellar_transaction_id"] = closeTxId

		_, err = appContext.Client.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
		if err != nil {
			log.Println("Save grayll-system algo position data error: ", docPath, err)
			appContext.ReconnectFireStoreSys()
			_, err = appContext.Client.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
			if err != nil {
				return err
			}

		}

		_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
		if err != nil {
			log.Println("Save grayll-app algo position data error: ", err)
			appContext.ReconnectFireStoreApp()
			_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
			if err != nil {
				log.Println("Save grayll-app algo position data retried: ", err)
				return err
			}
		}

		docref, err := appContext.GrayllAppClient.Doc(docPath).Get(ctx)
		if err != nil {
			log.Println("[ERROR] Get grayll-gry1 algo position data error: ", docPath, err)
			appContext.ReconnectFireStoreApp()
			docref, err = appContext.GrayllAppClient.Doc(docPath).Get(ctx)
			if err == nil {
				algoliaIndex := docref.Data()
				// index algolia algo position
				algoliaIndex["id"] = grayll_tx_id
				_, err = appContext.AlgoliaIndex.SaveObject(algoliaIndex)
				if err != nil {
					log.Println("algoliaTransferIndex error:", err)
				}
			}
		} else {
			algoliaIndex := docref.Data()
			// index algolia algo position
			algoliaIndex["id"] = grayll_tx_id
			_, err = appContext.AlgoliaIndex.SaveObject(algoliaIndex)
			if err != nil {
				log.Println("algoliaTransferIndex error:", err)
			}
		}

		algorithm := algoData["algorithm_type"].(string)
		algorithmShort := strings.ToLower(strings.ReplaceAll(algorithm, " ", ""))
		appContext.Cache.DelTxProcess(user_id, grayll_tx_id)
		appContext.Cache.RemoveRoi(algorithmShort, grayll_tx_id)

		// Notice and mail
		position := AlgorithmPosition{}
		err = json.Unmarshal(msg.Data, &position)
		if err != nil {
			log.Println("json.Unmarshal error: ", err)
			return err
		}

		// Save notice for user and update unread message
		cacheTxId, unreadPath := GetCacheTxId(algorithm, grayll_tx_id)

		_, err = appContext.GrayllAppClient.Doc("users_meta/"+user_id).Update(ctx, []firestore.Update{
			{Path: unreadPath, Value: firestore.Increment(1)},
		})
		if err != nil {
			log.Println("Unread update error: ", unreadPath, err, cacheTxId)
			//return
		}

		// Update metric data in cache
		openPositions, err := appContext.Cache.UpdatePositionClose(user_id, algorithm, grayll_tx_id)

		currentRoi, totalCurrentValue := appContext.Cache.GetCurrentValues(user_id, algorithm, true)
		str := strings.ToLower(strings.ReplaceAll(position.AlgorithmType, " ", ""))
		key2 := fmt.Sprintf("total_%s_open_positions", str)
		key := fmt.Sprintf("total_%s_current_position_ROI_$", str)
		key1 := fmt.Sprintf("total_%s_current_position_value_$", str)
		//log.Printf("CLOSE-key: %v, key1: %v, currentRoi: %v, totalCurrentValue: %v\n", key, key1, currentRoi, totalCurrentValue)

		if totalCurrentValue >= 0 && openPositions >= 0 {
			appContext.GrayllAppClient.Doc("users_meta/"+user_id).Set(ctx, map[string]interface{}{
				key:  currentRoi,
				key1: totalCurrentValue,
				key2: openPositions,
			}, firestore.MergeAll)
		}
		appContext.GrayllAppClient.Doc("users_meta/"+user_id).Update(ctx, []firestore.Update{
			{Path: fmt.Sprintf("total_%s_close_positions_ROI_$", str), Value: firestore.Increment(algoData["close_position_ROI_$"].(float64))},
		})

		position.CloseStellarTxId = closeTxId
		title, content, url, contents := GenClosePositionGRZ(position)
		notice := map[string]interface{}{
			"title":       title,
			"body":        content,
			"isRead":      false,
			"type":        position.AlgorithmType,
			"time":        position.ClosePositionTimestamp,
			"url":         strings.Replace(url, "Stellar | Transaction ID | ", "", 1),
			"stellarTxId": closeTxId,
		}
		docRef := appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
		_, err = docRef.Set(ctx, notice)
		if err != nil {
			appContext.ReconnectFireStoreApp()
			docRef = appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
			_, err = docRef.Set(ctx, notice)
			if err != nil {
				log.Println("SaveNotice error: ", err)
				//return err
			}
		}

		// Mail wallet
		go func() {
			mailAlgo, err := appContext.Cache.GetNotice(user_id, "MailAlgo")
			if err == nil {
				// check setting and send mail
				if mailAlgo == "1" {
					//log.Println("Start send mail notice")
					userRef, err := appContext.GrayllAppClient.Doc("users/" + user_id).Get(ctx)
					if userRef != nil {
						err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
						if err != nil {
							log.Println("SendNoticeMail error: ", err)
						}
					}
				}
			}

			// REFERER Check referer
			if val, ok := algoData["close_perf_fee_referer_GRX"]; ok {
				refererFee := val.(float64)
				if refererFee > 0 {
					refererId := algoData["referer_id"].(string)

					log.Println("refererFee: ", refererFee)
					batch := appContext.GrayllAppClient.Batch()
					userRef, err := appContext.GrayllAppClient.Doc("users/" + refererId).Get(ctx)
					if userRef == nil {
						log.Println("Can not get referer information:", refererId)
						return

					}
					// Update referral transaction
					referralContact, err := appContext.GrayllAppClient.Doc("referrals/" + refererId + "/referral/" + user_id).Get(ctx)
					if err != nil {
						// Can not find the referral of refererId
						log.Println("Can not get referral information:", user_id, refererId)
						return
					}

					feeStellarTx, err := SendGrxMemo(refererFee, userRef.Data()["PublicKey"].(string),
						appContext.Config.HotWalletTwoSeed, appContext.Asset, grayll_tx_id)
					if err != nil {
						log.Println("SendGrx error: ", err)
					}

					feeTxData := map[string]interface{}{
						"time":  time.Now().Unix(),
						"name":  referralContact.Data()["name"].(string),
						"lname": referralContact.Data()["lname"].(string),
						//"businessName": "", //referralContact.Data()["businessName"].(string),
						//"phone":        "", //referralContact.Data()["phone"].(string),
						"email":     referralContact.Data()["email"].(string),
						"feeGRX":    refererFee,
						"stellarTx": feeStellarTx,
						"grayllTx":  grayll_tx_id,
					}
					feeTxDataDoc := appContext.GrayllAppClient.Collection("referrals/" + refererId + "/txs").NewDoc()
					batch.Set(feeTxDataDoc, feeTxData)

					// Increase feeGRX in referral contact
					batch.Update(referralContact.Ref, []firestore.Update{
						{Path: "totalFeeGRX", Value: firestore.Increment(refererFee)},
						{Path: "totalPayment", Value: firestore.Increment(1)},
					})
					metricDoc := appContext.GrayllAppClient.Doc("referrals/" + refererId + "/metrics/referral")
					batch.Update(metricDoc, []firestore.Update{
						{Path: "totalFeeGRX", Value: firestore.Increment(refererFee)},
						{Path: "totalPayment", Value: firestore.Increment(1)},
					})

					refererDoc := appContext.GrayllAppClient.Doc("referrals/" + user_id + "/referer/" + refererId)
					batch.Update(refererDoc, []firestore.Update{
						{Path: "feeGRX", Value: firestore.Increment(refererFee)},
					})
					grxusd, err := appContext.Cache.GetGRXUsd()
					if err != nil {

					}
					title, content, url, contents := mail.GenRefererProfit(referralContact.Data()["name"].(string),
						referralContact.Data()["lname"].(string), feeStellarTx, grayll_tx_id, refererFee, grxusd)
					notice := map[string]interface{}{
						"title":       title,
						"body":        content,
						"isRead":      false,
						"type":        position.AlgorithmType,
						"time":        position.ClosePositionTimestamp,
						"url":         strings.Replace(url, "Stellar | Transaction ID | ", "", 1),
						"stellarTxId": feeStellarTx,
					}
					docRef := appContext.GrayllAppClient.Collection("notices/algo/" + refererId).NewDoc()
					batch.Create(docRef, notice)
					//_, err = docRef.Set(ctx, notice)

					//_, err = batch.Commit(ctx)
					if err != nil {
						log.Println("SaveNotice error: ", err)
						return
					}

					mailAlgoReferer, err := appContext.Cache.GetNotice(refererId, "MailAlgo")
					if err == nil {
						// check setting and send mail
						if mailAlgoReferer == "1" {
							err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
							if err != nil {
								log.Println("SendNoticeMail error: ", err)
							}
						}
					} else {
						log.Println("Can not get MailWallet setting from cache:", err)
					}

					_, err = batch.Commit(ctx)
					if err != nil {
						log.Println("Referer batch commit error: ", err)
					}
					//algoliaIndex := feeTxData
					// index algolia algo position
					feeTxData["user_id"] = refererId
					_, err = appContext.AlgoliaReferralIndex.SaveObject(feeTxData, opt.AutoGenerateObjectIDIfNotExist(true))
					if err != nil {
						log.Println("AlgoliaReferralIndex save error:", err)
					}
				}
			}
		}()

		// Update close stellar tx id and status for grz-arkady
		_, err = appContext.GrzClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
		if err != nil {
			log.Println("[ERROR] Save grayll-grz-arkady algo position data error: ", docPath, err)
			appContext.ReconnectFireStoreGryz()
			_, err = appContext.GrzClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
			if err != nil {
				log.Println("[ERROR] Save grayll-grz-arkady algo position data error: ", docPath, err)
			}
		}

	} else if action == "CLOSEALL" {
		var input AlgoCloseAllInputData
		err := json.Unmarshal(msg.Data, &input)
		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ close all position request's body, error : %v", err)
			//GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
			return err
		}
		// Check firestore connection
		err = appContext.CheckClientConnection()
		if err != nil {
			msg.Nack()
			log.Fatalln("[ERROR] Can not connect to firestore:", err)
		}
		var totalAmount float64 = 0
		urGRZ := 0
		urGRY1 := 0
		urGRY2 := 0
		urGRY3 := 0
		userId := ""
		totalFeeReferer := float64(0)
		refererId := input.Data[0].RefererId
		memo := strings.TrimLeft(input.Data[len(input.Data)-1].GrayllTxId, "0") + input.Data[len(input.Data)-1].AlgorithmType
		for _, position := range input.Data {
			totalAmount += position.ClosePositionValueGRX
			if position.ClosePerfFeeRefererGRX > 0 {
				totalFeeReferer += position.ClosePerfFeeRefererGRX
			}
			switch position.AlgorithmType {
			case "GRZ":
				urGRZ++
			case "GRY 1":
				urGRY1++
			case "GRY 2":
				urGRY2++
			case "GRY 3":
				urGRY3++
			}
			userId = position.UserId
		}

		userAddress, err := appContext.Cache.GetPublicKeyFromUid(userId)
		if err != nil {
			log.Println("[ERROR] Can not get public key from uid", userId, err)
			appContext.ReconnectRedis()
			userAddress, err = appContext.Cache.GetPublicKeyFromUid(userId)
			if err != nil {
				log.Println("[ERROR] Can not get public key from uid retried", userId, err)
				msg.Nack()
				return err
			}
		}

		batchClient := appContext.Client.Batch()
		batchGrayllApp := appContext.GrayllAppClient.Batch()

		closeTxId, err := SendGrxMemo(totalAmount, userAddress, appContext.Config.HotWalletTwoSeed, appContext.Asset, memo)
		if err != nil {
			log.Println("[ERROR] Can send from hotwallet2 to user", userId, userAddress, err)
			msg.Nack()
			return err
		} else {
			msg.Ack()
		}

		txFeeReferer := ""
		if refererId != "" {
			refererAddress, err := appContext.Cache.GetPublicKeyFromUid(refererId)
			if err != nil {
				log.Println("Can not get public key from uid", refererId, err)
				return err
			}
			if totalFeeReferer > 0 {
				txFeeReferer, err = SendGrxMemo(totalFeeReferer, refererAddress,
					appContext.Config.HotWalletTwoSeed, appContext.Asset, memo)
				if err != nil {
					log.Println("Can send to referer", refererAddress, err)
				} else {
					log.Println("txFeeReferer:", txFeeReferer)
					refererDoc := appContext.GrayllAppClient.Doc("referrals/" + userId + "/referer/" + refererId)
					batchGrayllApp.Update(refererDoc, []firestore.Update{
						{Path: "feeGRX", Value: firestore.Increment(totalFeeReferer)},
					})
					referralDoc := appContext.GrayllAppClient.Doc("referrals/" + refererId + "/referral/" + userId)
					batchGrayllApp.Update(referralDoc, []firestore.Update{
						{Path: "totalFeeGRX", Value: firestore.Increment(totalFeeReferer)},
						{Path: "totalPayment", Value: firestore.Increment(1)},
					})
					metricDoc := appContext.GrayllAppClient.Doc("referrals/" + refererId + "/metrics/referral")
					batchGrayllApp.Update(metricDoc, []firestore.Update{
						{Path: "totalFeeGRX", Value: firestore.Increment(totalFeeReferer)},
						{Path: "totalPayment", Value: firestore.Increment(1)},
					})
				}

			}
		}

		log.Println("Send back user with ledger", userAddress, closeTxId)

		var userRef *firestore.DocumentSnapshot
		mailAlgo, err := appContext.Cache.GetNotice(userId, "MailAlgo")
		if err == nil {
			// check setting and send mail
			if mailAlgo == "1" {
				//log.Println("Start send mail notice")
				userRef, err = appContext.GrayllAppClient.Doc("users/" + userId).Get(ctx)
			}
		}

		algorithms := []AlgorithmData{
			AlgorithmData{AlgorithmType: "GRZ", PositionRoi: float64(0)}, AlgorithmData{AlgorithmType: "GRY1", PositionRoi: float64(0)},
			AlgorithmData{AlgorithmType: "GRY2", PositionRoi: float64(0)}, AlgorithmData{AlgorithmType: "GRY3", PositionRoi: float64(0)}}

		algoliaIndies := make([]map[string]interface{}, 0)
		algoliaRefIndies := make([]map[string]interface{}, 0)
		mailAlgoReferer, err := appContext.Cache.GetNotice(refererId, "MailAlgo")
		grxusd, err := appContext.Cache.GetGRXUsd()
		grayllTxs := make([]string, 0)

		for _, position := range input.Data {
			user_id := position.UserId
			grayll_tx_id := position.GrayllTxId

			if user_id != userId {
				log.Println("Invalid user_id empty")
				return errors.New("invalid userid or grayll tx id")
			}
			grayllTxs = append(grayllTxs, grayll_tx_id)
			docPath := fmt.Sprintf("algo_positions/users/%s/%s", user_id, grayll_tx_id)

			closeTime := time.Now().Unix()
			position.CloseStellarTxId = closeTxId
			position.ClosePositionTimestamp = closeTime

			position.Duration = closeTime - position.OpenPositionTimestamp
			position.DurationString = ConvertSectoDay(position.Duration)

			position.Status = "CLOSED"
			position.CloseStellarTxId = closeTxId

			// Begin
			switch position.AlgorithmType {
			case "GRZ":
				algorithms[0].PositionRoi += position.ClosePositionROIUSD
				algorithms[0].PositionsClosed += 1
			case "GRY 1":
				algorithms[1].PositionRoi += position.ClosePositionROIUSD
				algorithms[1].PositionsClosed += 1
			case "GRY 2":
				algorithms[2].PositionRoi += position.ClosePositionROIUSD
				algorithms[2].PositionsClosed += 1
			case "GRY 3":
				algorithms[3].PositionRoi += position.ClosePositionROIUSD
				algorithms[3].PositionsClosed += 1
			}
			// Remove cache for transacation id
			appContext.Cache.UpdatePositionCloseAll(user_id, position.AlgorithmType, grayll_tx_id)
			// End

			_position, err := json.Marshal(&position)
			if err != nil {
				log.Println("[ERROR] get algo position data on grayll-system: ", docPath, err)
			}

			positionMap := make(map[string]interface{})
			err = json.Unmarshal(_position, &positionMap)
			if err != nil {
				log.Println("[ERROR] unmarshal position data on grayll-system: ", docPath, err)
			}
			// _, err = appContext.Client.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
			// if err != nil {
			// 	log.Println("Save grayll-system algo position data error: ", docPath, err)
			// 	return err
			// }
			systemDoc := appContext.Client.Doc(docPath)
			batchClient.Set(systemDoc, positionMap, firestore.MergeAll)

			docPath1 := fmt.Sprintf("algo_position_values/graylltxs/%s", grayll_tx_id)
			systemPosValuedoc := appContext.Client.Doc(docPath1)
			batchClient.Delete(systemPosValuedoc)

			grayAppDocRef := appContext.GrayllAppClient.Doc(docPath)
			batchGrayllApp.Set(grayAppDocRef, positionMap, firestore.MergeAll)

			docref, err := appContext.Client.Doc(docPath).Get(ctx)
			if err != nil {
				log.Println("[ERROR] Get grayll-gry1 algo position data error: ", docPath, err)
			} else {
				algoliaIndex := docref.Data()
				// index algolia algo position
				algoliaIndex["id"] = grayll_tx_id
				algoliaIndies = append(algoliaIndies, algoliaIndex)
			}

			// Notice and mail
			title, content, url, contents := GenCloseAllPositionGRZ(position)
			notice := map[string]interface{}{
				"title":       title,
				"body":        content,
				"isRead":      false,
				"type":        position.AlgorithmType,
				"time":        position.ClosePositionTimestamp,
				"url":         strings.Replace(url, "Stellar | Transaction ID | ", "", 1),
				"stellarTxId": closeTxId,
			}

			noticeRef := appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
			batchGrayllApp.Create(noticeRef, notice)

			if err != nil {
				log.Println("SaveNotice error: ", err)
				return err
			}

			// Mail wallet
			go func() {
				if userRef != nil {
					err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
					if err != nil {
						log.Println("SendNoticeMail error: ", err)
					}
				}
			}()

			// Update close stellar tx id and status for grz-arkady
			_, err = appContext.GrzClient.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
			if err != nil {
				log.Println("[ERROR] Save grayll-grz-arkady algo position data error: ", docPath, err)
				appContext.ReConnectFireStore(err.Error(), 3)
				_, err = appContext.GrzClient.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
				if err != nil {
					log.Println("[ERROR] Save grayll-grz-arkady algo position data error retry: ", docPath, err)
				}
			}

			// REFERER Check referer
			refererFee := position.ClosePerfFeeRefererGRX
			if refererFee > 0 {

				log.Println("refererFee: ", refererFee)
				//batch := appContext.GrayllAppClient.Batch()
				userRef, err := appContext.GrayllAppClient.Doc("users/" + refererId).Get(ctx)
				if userRef == nil {
					log.Println("Can not get referer information:", refererId)

				}
				// Update referral transaction
				referralContact, err := appContext.GrayllAppClient.Doc("referrals/" + refererId + "/referral/" + user_id).Get(ctx)
				if err != nil {
					// Can not find the referral of refererId
					log.Println("Can not get referral information:", user_id, refererId)
				}

				feeTxData := map[string]interface{}{
					"time":  time.Now().Unix(),
					"name":  referralContact.Data()["name"].(string),
					"lname": referralContact.Data()["lname"].(string),
					//"businessName": referralContact.Data()["businessName"].(string),
					//"phone":        referralContact.Data()["phone"].(string),
					"email":     referralContact.Data()["email"].(string),
					"feeGRX":    refererFee,
					"stellarTx": txFeeReferer,
					"grayllTx":  grayll_tx_id,
				}
				feeTxDataDoc := appContext.GrayllAppClient.Collection("referrals/" + refererId + "/txs").NewDoc()
				batchGrayllApp.Set(feeTxDataDoc, feeTxData)

				title, content, url, contents := mail.GenRefererProfit(referralContact.Data()["name"].(string),
					referralContact.Data()["lname"].(string), txFeeReferer, grayll_tx_id, refererFee, grxusd)

				// title, content, contents := mail.GenRefererProfit(referralContact.Data()["name"].(string), referralContact.Data()["lname"].(string),
				// 	txFeeReferer, position.ClosePerfFeeRefererGRX, grxusd)
				notice := map[string]interface{}{
					"title":       title,
					"body":        content,
					"isRead":      false,
					"type":        position.AlgorithmType,
					"time":        position.ClosePositionTimestamp,
					"url":         strings.Replace(url, "Stellar | Transaction ID | ", "", 1),
					"stellarTxId": txFeeReferer,
				}
				docRef := appContext.GrayllAppClient.Collection("notices/algo/" + refererId).NewDoc()
				batchGrayllApp.Create(docRef, notice)

				if mailAlgoReferer == "1" {
					err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
					if err != nil {
						log.Println("SendNoticeMail error: ", err)
					}
				}

				algoliaIndex := feeTxData
				// index algolia algo position
				algoliaIndex["id"] = grayll_tx_id
				algoliaIndex["user_id"] = refererId
				algoliaRefIndies = append(algoliaRefIndies, algoliaIndex)
			}

			//End REFERER
		}

		// Update metric data in cache
		updateAlgorithmData(userId, algorithms, appContext)
		// End metric cache

		userMetaRef := appContext.GrayllAppClient.Doc("users_meta/" + userId)
		batchGrayllApp.Update(userMetaRef, []firestore.Update{
			{Path: "UrGRZ", Value: firestore.Increment(urGRZ)},
			{Path: "UrGRY1", Value: firestore.Increment(urGRY1)},
			{Path: "UrGRY2", Value: firestore.Increment(urGRY2)},
			{Path: "UrGRY3", Value: firestore.Increment(urGRY3)},
		})
		if err != nil {
			log.Println("Unread update unread number error: ", err)
			//return
		}
		_, err = batchClient.Commit(ctx)
		if err != nil {
			log.Println("[ERROR] commit batchClient: ", err)
			appContext.ReconnectFireStoreSys()
			_, err = batchClient.Commit(ctx)
			if err != nil {
				log.Println("[ERROR] commit batchClient retry: ", err)
			}
		}
		_, err = batchGrayllApp.Commit(ctx)
		if err != nil {
			log.Println("[ERROR] commit batchGrayllApp: ", err)
			appContext.ReconnectFireStoreApp()
			_, err = batchGrayllApp.Commit(ctx)
			if err != nil {
				log.Println("[ERROR] commit batchGrayllApp retry: ", err)
			}
		}
		if err == nil {
			_, err = appContext.AlgoliaIndex.SaveObjects(algoliaIndies, opt.AutoGenerateObjectIDIfNotExist(true))
			if err != nil {
				log.Println("AlgoliaIndex save error:", err)
			}
			if len(algoliaRefIndies) > 0 {
				_, err = appContext.AlgoliaReferralIndex.SaveObjects(algoliaRefIndies, opt.AutoGenerateObjectIDIfNotExist(true))
				if err != nil {
					log.Println("AlgoliaReferralIndex save error:", err)
				}
			}
			algo := strings.ToLower(strings.ReplaceAll(input.Data[0].AlgorithmType, " ", ""))
			appContext.Cache.RemoveRoi(algo, grayllTxs...)
			appContext.Cache.DelTxProcess(userId, grayllTxs...)
		}

	} else if action == "UPDATE" {
		msg.Ack()
		updateTime := int64(algoData["last_update_at"].(float64))
		currentTs := time.Now().Unix()
		user_id := algoData["user_id"].(string)
		grayll_tx_id := algoData["grayll_transaction_id"].(string)

		lastUpdateAt := time.Unix(updateTime, 0).Format("2006-01-02 15:04:05")
		currentTime := time.Unix(currentTs, 0).Format("2006-01-02 15:04:05")
		if updateTime < currentTs-120 {
			log.Printf("%s - %s - Reject old task at: %s. Scheduled at: %s", user_id, grayll_tx_id, currentTime, lastUpdateAt)
			return nil
		}

		// user_id := algoData["user_id"].(string)
		// grayll_tx_id := algoData["grayll_transaction_id"].(string)
		algorithm := "GRZ"
		// if val, ok := algoData["algorithm_type"]; ok {
		// 	algorithm = val.(string)
		// }

		if user_id == "" {
			log.Println("Invalid user_id empty")
			return errors.New("invalid userid or grayll tx id")
		}
		delete(algoData, "status")

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", user_id, grayll_tx_id)
		_, err = appContext.Client.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
		if err != nil {
			log.Println("Save grayll-system algo position data error: ", docPath, err)
			appContext.ReconnectFireStoreSys()
			_, err = appContext.Client.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
			if err != nil {
				log.Println("ERROR UPDATE - Save grayll-system algo position data error retry: ", docPath, err)
			}
		}

		currentValue := algoData["current_position_value_$"].(float64)
		currentRoiPercent := algoData["current_position_ROI_percent"].(float64)
		duration := int64(algoData["duration"].(float64))

		step := duration / 60

		// Update metric data in cache
		appContext.Cache.UpdateCurrentPositionValues(user_id,
			algorithm, grayll_tx_id, algoData["current_position_ROI_$"].(float64), currentValue)

		currentRoi, totalCurrentValue := appContext.Cache.GetCurrentValues(user_id, algorithm, true)
		str := strings.ToLower(strings.ReplaceAll(algorithm, " ", ""))
		key := fmt.Sprintf("total_%s_current_position_ROI_$", str)
		key1 := fmt.Sprintf("total_%s_current_position_value_$", str)
		appContext.GrayllAppClient.Doc("users_meta/"+user_id).Set(ctx, map[string]interface{}{
			key:  currentRoi,
			key1: totalCurrentValue,
		}, firestore.MergeAll)
		// End metric cache

		_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
		if err != nil {
			log.Println("ERROR UPDATE - Save grayll-app algo position data error: ", err)
			appContext.ReconnectFireStoreApp()
			_, err = appContext.GrayllAppClient.Doc(docPath).Set(ctx, algoData, firestore.MergeAll)
			if err != nil {
				log.Println("ERROR UPDATE - Save grayll-app algo position data error retry: ", err)
			}
		}

		algoShort := strings.ToLower(strings.ReplaceAll(algorithm, " ", ""))
		grzUsd := algoData["current_value_GRZ"].(float64)

		//Calculte roi 24h, 7d, total
		if step%5 == 0 {
			updateTime := int64(algoData["last_update_at"].(float64))
			if step <= TIME_24H_MINUTES {
				appContext.Cache.UpdateRoi(grayll_tx_id, algoShort, currentRoiPercent, "all")
				log.Println("update all roi:", currentRoiPercent)
			} else {
				// update every 5minute
				// Update roi total, roi total is current position roi
				appContext.Cache.UpdateRoi(grayll_tx_id, algoShort, currentRoiPercent, "total")

				// Calculate 24h roi and 7d roi
				delta := updateTime - TIME_24H_SECONDS
				frame1m := "asset_algo_values/grzusd/frame_01m"
				docs, _ := appContext.GrayllAppClient.Collection(frame1m).Where("UNIX_timestamp", ">=", delta).OrderBy("UNIX_timestamp", firestore.Asc).Limit(1).Documents(ctx).GetAll()
				if docs != nil && len(docs) > 0 {
					last24hvalue := docs[0].Data()["price"].(float64)
					roi24h := (grzUsd - last24hvalue) * 100 / last24hvalue
					log.Println("update roi24h:", roi24h, delta)
					log.Println("update roi24h doc:", docs[0].Data())
					appContext.Cache.UpdateRoi(grayll_tx_id, algoShort, roi24h, "24h")
				}

				if step > TIME_7D_MINUTES {
					delta = updateTime - TIME_7D_SECONDS
					docs, _ := appContext.Client.Collection(frame1m).Where("UNIX_timestamp", ">=", delta).OrderBy("UNIX_timestamp", firestore.Asc).Limit(1).Documents(ctx).GetAll()
					if docs != nil && len(docs) > 0 {
						last7dvalue := docs[0].Data()["price"].(float64)
						roi7d := (grzUsd - last7dvalue) * 100 / last7dvalue
						log.Println("update roi7d:", roi7d)
						appContext.Cache.UpdateRoi(grayll_tx_id, algoShort, roi7d, "7d")
					}
				} else {
					// is current roi
					log.Println("update roi7d is current roi:", currentRoiPercent)
					appContext.Cache.UpdateRoi(grayll_tx_id, algoShort, currentRoiPercent, "7d")
				}
			}
		}

	} else if action == "PERFORMANCE" {

		user_id := algoData["user_id"].(string)

		if user_id == "" {
			log.Println("Invalid user_id empty")
			return errors.New("invalid userid or grayll tx id")
		}
		// Check firestore connection
		_, err = appContext.GrayllAppClient.Doc(CheckConnectionPath).Get(ctx)
		if err != nil {
			log.Println("[ERROR] check grayll-app connection error: ", CheckConnectionPath, err)
			appContext.ReconnectFireStoreApp()
			_, err := appContext.GrayllAppClient.Doc(CheckConnectionPath).Get(ctx)
			if err != nil {
				msg.Nack()
				return err
			}
		}

		algorithm := "GRZ"

		openPositionTime := int64(algoData["open_position_timestamp"].(float64))
		duration := time.Now().Unix() - openPositionTime
		durationString := ConvertSectoDay(duration)
		title, content, _, contents := GenPerformanceChange(algoData, durationString, GetAlgorithmString(algorithm))
		notice := map[string]interface{}{
			"title":  title,
			"body":   content,
			"isRead": false,
			"type":   algorithm,
			"time":   time.Now().Unix(),
			"url":    "",
		}
		docRef := appContext.GrayllAppClient.Collection("notices").Doc("algo").Collection(user_id).NewDoc()
		_, err = docRef.Set(ctx, notice)

		if err != nil {
			log.Println("SaveNotice error: ", err)
			appContext.ReconnectFireStoreApp()
			_, err = docRef.Set(ctx, notice)
			if err != nil {
				return err
			}
		}

		// Mail wallet
		go func() {
			userRef, err := appContext.GrayllAppClient.Doc("users/" + user_id).Get(ctx)
			if userRef != nil {
				err = SendNoticeMail(userRef.Data()["Email"].(string), userRef.Data()["Name"].(string), title, contents)
				if err != nil {
					log.Println("SendNoticeMail error: ", err)
				}
				_, err = appContext.GrayllAppClient.Doc("users_meta/"+user_id).Update(ctx, []firestore.Update{
					{Path: getUnReadPath(algorithm), Value: firestore.Increment(1)},
				})
			}
		}()
		msg.Ack()
	}

	return nil
}

func getUnReadPath(algorithm string) string {
	switch algorithm {
	case "GRZ":
		return "UrGRZ"
	case "GRY 1":
		return "UrGRY1"
	case "GRY 2":
		return "UrGRY2"
	case "GRY 3":
		return "UrGRY3"
	}
	return ""
}

func updateAlgorithmData(userId string, algorithms []AlgorithmData, appContext *AppContext) {
	ctx := context.Background()
	for _, algorithmData := range algorithms {
		if algorithmData.PositionsClosed > 0 {
			currentRoi, totalCurrentValue := appContext.Cache.GetCurrentValues(userId, algorithmData.AlgorithmType, true)
			str := strings.ToLower(strings.ReplaceAll(algorithmData.AlgorithmType, " ", ""))
			key2 := fmt.Sprintf("total_%s_open_positions", str)
			key := fmt.Sprintf("total_%s_current_position_ROI_$", str)
			key1 := fmt.Sprintf("total_%s_current_position_value_$", str)
			log.Printf("CLOSE-key: %v, key1: %v, currentRoi: %v, totalCurrentValue: %v\n", key, key1, currentRoi, totalCurrentValue)

			log.Println("algorithmData:", algorithmData)
			log.Println("currentRoi:", currentRoi)
			log.Println("totalCurrentValue:", totalCurrentValue)
			// Reset position numbers
			appContext.Cache.UpdatePositionNumbers(userId, algorithmData.AlgorithmType, "CLOSE", 0)

			appContext.GrayllAppClient.Doc("users_meta/"+userId).Set(ctx, map[string]interface{}{
				key:  currentRoi,
				key1: totalCurrentValue,
				key2: 0,
			}, firestore.MergeAll)

			// appContext.Client.Doc("users_meta/"+userId).Set(ctx, map[string]interface{}{
			// 	key:  currentRoi,
			// 	key1: totalCurrentValue,
			// 	key2: 0,
			// }, firestore.MergeAll)

			appContext.GrayllAppClient.Doc("users_meta/"+userId).Update(ctx, []firestore.Update{
				{Path: fmt.Sprintf("total_%s_close_positions_ROI_$", str), Value: firestore.Increment(algorithmData.PositionRoi)},
			})
			// appContext.Client.Doc("users_meta/"+userId).Update(ctx, []firestore.Update{
			// 	{Path: fmt.Sprintf("total_%s_close_positions_ROI_$", str), Value: firestore.Increment(algorithmData.PositionRoi)},
			// })
		}
	}

}

func GetAlgorithmString(algoType string) string {
	algoString := ""
	switch algoType {
	case "GRZ":
		algoString = "GRZ Arkady"
		break
	case "GRY 1":
		algoString = "GRY1 Balthazar"
		break
	case "GRY 2":
		algoString = "GRY2 Kaspar"
		break
	case "GRY 3":
		algoString = "GRY3 Melkior"
		break

	}
	return algoString
}
func GetCacheTxId(algoType, grayll_tx_id string) (string, string) {
	cacheTxId := ""
	unreadPath := ""
	switch algoType {
	case "GRZ":
		unreadPath = "UrGRZ"
		cacheTxId = "z1" + grayll_tx_id
	case "GRY 1":
		unreadPath = "UrGRY1"
		cacheTxId = "y1" + grayll_tx_id
	case "GRY 2":
		unreadPath = "UrGRY2"
		cacheTxId = "y2" + grayll_tx_id
	case "GRY 3":
		unreadPath = "UrGRY3"
		cacheTxId = "y3" + grayll_tx_id
	}
	return cacheTxId, unreadPath
}

func ProcessMvpData(ctx context.Context, appContext *AppContext, msg *pubsub.Message) error {

	priceh := PriceH{}
	err := json.Unmarshal(msg.Data, &priceh)
	if err != nil {
		log.Println("json.Unmarshal error: ", err)
		return err
	}

	batch := appContext.Client.Batch()
	docPath := fmt.Sprintf("asset_algo_values/%s/%s", priceh.GRType, priceh.Frame)
	docRef := appContext.Client.Collection(docPath).NewDoc()
	batch.Create(docRef, map[string]interface{}{
		"price":          priceh.Price,
		"UNIX_timestamp": priceh.UnixTs,
	})
	logger.Info("ADD ASSET-ALGO PRICE: ", priceh.GRType, priceh.Frame, time.Unix(priceh.UnixTs, 0).Format("2006-01-02 15:04:05"), priceh.Price)
	if priceh.Frame == "frame_01m" && (priceh.GRType == "gryusd" || priceh.GRType == "grzusd") {
		logger.Info("PUBLISH PRICE: ", priceh.GRType, priceh.Frame, time.Unix(priceh.UnixTs, 0).Format("2006-01-02 15:04:05"))
		priceStr := priceh.GRType[:3] + "p"
		priceStrNew := priceh.GRType[:3] + "usd"
		docRefPrice := appContext.Client.Doc("prices/794retePzavE19bTcMaH")
		batch.Set(docRefPrice, map[string]interface{}{
			priceStr:    priceh.Price,
			priceStrNew: priceh.Price,
		}, firestore.MergeAll)
	}
	// _, err = batch.Commit(ctx)
	// if err != nil {
	// 	log.Println("Batch commit error: ", err)
	// 	return err
	// }
	return nil
}
func ReconnectFireStore(projectId string, timeout int) (*firestore.Client, error) {
	cnt := 0
	var client *firestore.Client
	var err error
	ctx := context.Background()
	for {
		cnt++
		time.Sleep(1 * time.Second)
		// conf := &firebase.Config{ProjectID: projectId}

		// app, err := firebase.NewApp(ctx, conf)
		// if err != nil {
		// 	return nil, err
		// }
		// client, err = app.Firestore(ctx)
		client, err = firestore.NewClient(ctx, projectId)

		if err == nil {
			break
		}
		if cnt > timeout {
			log.Println("[ERROR] Can not connect to firestore after retry times", cnt, projectId, err)
			break
		}

	}
	return client, err
}
