package api

import (
	"context"
	"crypto/rand"
	"errors"
	"log"
	"strconv"

	"cloud.google.com/go/firestore"
	"github.com/mitchellh/mapstructure"

	//"bitbucket.org/grayll/grayll.io-grz-arkady/models"

	//"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	//"os"
	"encoding/json"

	"github.com/gin-gonic/gin"
	"github.com/stellar/go/clients/horizonclient"

	//"bitbucket.org/grayll/grayll.io-grz-arkady/utils"
	"github.com/stellar/go/protocols/horizon/operations"
)

func MapToStruct(params interface{}, m map[string]interface{}) error {
	cfg := &mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   params,
		TagName:  "json",
	}
	decoder, _ := mapstructure.NewDecoder(cfg)
	return decoder.Decode(m)
}

func GinRespond(c *gin.Context, status int, errCode, msg string) {
	c.JSON(status, gin.H{
		"errCode": errCode, "msg": msg,
	})
	c.Abort()
}

type Price struct {
	N string `json:"n"`
	D string `json:"d"`
}
type PriceTime struct {
	P   float64 `json:"p"`
	UTS int64   `json:"uts"`
}
type Embedded struct {
	Records []map[string]interface{} `json:"records"`
}
type LedgerPayment struct {
	Embed Embedded `json:"_embedded"`
}

func ParsePaymentFromTxHash(txHash string, client *horizonclient.Client) ([]operations.Payment, error) {

	opRequest := horizonclient.OperationRequest{ForTransaction: txHash}
	ops, err := client.Operations(opRequest)

	payments := make([]operations.Payment, 0)
	if err != nil {
		return payments, err
	}
	for _, record := range ops.Embedded.Records {

		payment, ok := record.(operations.Payment)
		if ok {
			payments = append(payments, payment)
		}

	}
	return payments, nil
}

func ParseLedgerData(url string) (*LedgerPayment, error) {
	ledger := LedgerPayment{}

	res, err := http.Get(url)
	if err != nil {
		log.Println("http.Get "+url+" error:", err)
		return nil, err
	}

	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	//log.Println("data:", string(data))

	err = json.Unmarshal(data, &ledger)
	if err != nil {
		return nil, err
	}
	return &ledger, nil

}

func GetLedgerInfo(url, publicKey string) (string, string, float64, error) {
	//"https://horizon-testnet.stellar.org/ledgers/1072717/payments"
	em, err := ParseLedgerData(url)
	if err != nil {
		log.Println("ParseLedgerData err:", err)
		return "", "", 0, err
	}
	//log.Println("GetLedgerInfo-em", em)

	for _, record := range em.Embed.Records {
		if from, ok := record["from"]; ok && from.(string) == publicKey {
			to, _ := record["to"]
			amount, _ := record["amount"]

			log.Println("from:", from)
			log.Println(to)
			log.Println(amount)

			a, err := strconv.ParseFloat(amount.(string), 64)
			if err != nil {
				return "", "", 0, errors.New("Invalid ledger Id")
			}
			// if to.(string) == xlmLoaner {
			// 	return from.(string), to.(string), a, nil
			// }
			return from.(string), to.(string), a, nil
		}
	}
	return "", "", 0, errors.New("Invalid ledger Id")
}

func ConvertSectoDay(duration int64) string {
	day := duration / (24 * 3600)

	duration = duration % (24 * 3600)
	hour := duration / 3600

	duration %= 3600
	minutes := duration / 60

	duration %= 60
	//seconds := duration

	return fmt.Sprintf("%02d:%02d | %03d", hour, minutes, day)
}

//
// func GetPriceData(record map[string]interface{}, asset string) (PriceTime, error) {
// 	var n, d float64
// 	var err error
// 	if price, ok := record["price"]; ok {
// 		log.Println("price:", price)
// 		prices := price.(map[string]interface{})
// 		n1, ok1 := prices["n"]
// 		d1, ok2 := prices["d"]
// 		if ok1 && ok2 {
// 			n = n1.(float64)
// 			d = d1.(float64)
// 		}
// 	}
// 	var uts time.Time
// 	if closeTime, ok := record["ledger_close_time"]; ok {
// 		ts := closeTime.(string)
// 		uts, err = time.Parse("2006-01-02T15:04:05-0700", ts)
// 	}
// 	if asset == "xlm" {
// 		return PriceTime{P: n / d, UTS: uts.Unix()}, nil
// 	} else {
// 		return PriceTime{P: d / n, UTS: uts.Unix()}, nil
// 	}
//
// }

func GetPrice(url string) (float64, float64, error) {
	embs, err := ParseLedgerData(url)
	if err != nil {
		return 0, 0, err
	}

	if len(embs.Embed.Records) > 0 {
		if price, ok := embs.Embed.Records[0]["price"]; ok {
			//log.Println("price:", price)
			prices := price.(map[string]interface{})
			n, ok1 := prices["n"]
			d, ok2 := prices["d"]
			if ok1 && ok2 {
				return n.(float64), d.(float64), nil
			}
		}
	}
	return 0, 0, errors.New("price not found")
}

func GetPrices() {
	//grx xlm
	url := "https://horizon.stellar.org/trades?base_asset_type=native&counter_asset_type=credit_alphanum4&counter_asset_code=GRX&counter_asset_issuer=GAQQZMUNB7UCL2SXHU6H7RZVNFL6PI4YXLPJNBXMOZXB2LOQ7LODH333&order=desc&limit=1"
	n, d, err := GetPrice(url)
	log.Println(n, d, err, d/n)

	//xlm usd
	url = "https://horizon.stellar.org/trades?base_asset_type=native&counter_asset_type=credit_alphanum4&counter_asset_code=USD&counter_asset_issuer=GDUKMGUGDZQK6YHYA5Z6AY2G4XDSZPSZ3SW5UN3ARVMO6QSRDWP5YLEX&order=desc&limit=1"
	n, d, err = GetPrice(url)
	log.Println(n, d, err, n/d)
}

func randStr(strSize int, randType string) string {

	var dictionary string
	if randType == "alphanum" {
		dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}
	if randType == "alpha" {
		dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}
	if randType == "number" {
		dictionary = "0123456789"
	}
	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes)
}

const missingParam = "{missing : %v}"

func validateOpenPositionRequestBody(params AlgorithmPosition) error {
	if strings.EqualFold(params.UserId, "") {
		return fmt.Errorf(missingParam, "user_id")
	} else if strings.EqualFold(params.StellarTxId, "") {
		return fmt.Errorf(missingParam, "stellar_transaction_id")
	} else if params.OpenPositionTotalUSD <= 0.00001 {
		return fmt.Errorf("invalid %v param", "open_position_total_$")
	} else if params.OpenValueGRZ == 0 {
		return fmt.Errorf(missingParam, "grz_price_$")
	} else if params.OpenValueGRX == 0 {
		return fmt.Errorf(missingParam, "grx_price_$")
	} else if params.OpenPositionFeeUSD == 0 {
		return fmt.Errorf(missingParam, "open_position_fee_$")
	} else if params.OpenPositionFeeGRX == 0 {
		return fmt.Errorf(missingParam, "open_position_fee_GRX")
	} else if params.OpenPositionValueUSD == 0 {
		return fmt.Errorf(missingParam, "open_position_value_$")
	} else if params.OpenPositionTotalGRX == 0 {
		return fmt.Errorf(missingParam, "open_position_total_GRX")
	} else if params.OpenPositionValueGRZ == 0 {
		return fmt.Errorf(missingParam, "open_position_value_GRZ")
	} else if params.OpenPositionValueGRX == 0 {
		return fmt.Errorf(missingParam, "open_position_value_GRX")
	}

	return nil
}

func validateClosePositionRequestBody(params AlgorithmPosition) error {
	if strings.EqualFold(params.UserId, "") {
		return fmt.Errorf(missingParam, "user_id")
	} else if strings.EqualFold(params.GrayllTxId, "") {
		return fmt.Errorf(missingParam, "grayll_transaction_id")
	} else if params.CloseValueGRZ == 0 {
		return fmt.Errorf(missingParam, "grz_price_$")
	} else if params.CloseValueGRX == 0 {
		return fmt.Errorf(missingParam, "grx_price_$")
		// } else if params.ClosePositionTotalUSD == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_total_$")
		// } else if params.ClosePositionTotalGRX == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_total_GRX")
		// } else if params.ClosePositionValueGRZ == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_value_GRZ")
		// } else if params.ClosePositionFeeUSD == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_fee_$")
		// } else if params.ClosePositionValueUSD == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_value_$")
		// } else if params.ClosePositionValueGRX == 0 {
		// 	return fmt.Errorf(missingParam, "close_position_value_GRX")
	}
	return nil
}

func recursionCountDigits(number int64) int64 {
	if number < 10 {
		return 1
	}
	return 1 + recursionCountDigits(number/10)
}

func getOpenOrClosePositionFeeUSD(positionValueUSD float64) float64 {
	return (0.3 / 100) * positionValueUSD
}

// func getClosePerformanceFeeUSD(user_id, grayllTxId string, currentPositionValueUSD float64) (float64, error) {
// 	var currentPositionFeeUSD float64
// 	doc, err := client.Collection(fmt.Sprintf("algo_positions/%v", user_id)).
// 		Doc(grayllTxId).
// 		Get(context.Background())
// 	if err != nil {
// 		return 0, err
// 	}
// 	openingPositionValueUSD, ok := doc.Data()["open_position_value_$"].(float64)
// 	if !ok {
// 		return 0, fmt.Errorf("unable to get convert open_position_value_$ into float64"+
// 			"document with id # %v , open_position_value_$ : %v", grayllTxId, doc.Data()["open_position_value_$"])
// 	}

// 	if currentPositionValueUSD > openingPositionValueUSD {
// 		profit := (currentPositionValueUSD - currentPositionFeeUSD) - openingPositionValueUSD
// 		feeOnProfit := (1.8 / 100) * profit
// 		currentPositionFeeUSD += feeOnProfit
// 	}
// 	return currentPositionFeeUSD, nil
// }

// func createTask(projectID, locationID, queueID string, url string, message []byte) (*tasks.Task, error) {
// 	ctx := context.Background()
// 	client, err := cloudtasks.NewClient(ctx, option.WithCredentialsFile("grayll-grz.json"))
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer client.Close()

// 	queuePath := fmt.Sprintf("projects/%s/locations/%s/queues/%s", projectID, locationID, queueID)

// 	req := &tasks.CreateTaskRequest{
// 		Parent: queuePath,
// 		Task: &tasks.Task{
// 			//ScheduleTime: &timestamp.Timestamp{Seconds: time.Now().Unix() + 60},
// 			MessageType: &tasks.Task_HttpRequest{
// 				HttpRequest: &tasks.HttpRequest{
// 					HttpMethod: tasks.HttpMethod_POST,
// 					Url:        url,
// 					Body:       message,
// 				},
// 			},
// 		},
// 	}

// 	createdTask, err := client.CreateTask(ctx, req)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return createdTask, nil
// }

func getLastGrayllTxId(client *firestore.Client, ctx context.Context) (string, error) {

	var lastGrayllTxnID int64

	doc, err := client.Collection("Environment").Doc("jnMha28zlJc5cygik3cN").Get(ctx)
	if err != nil {
		return "", err
	}
	if doc.Data()["key"].(string) == "GrayllTxnID" {
		lastGrayllTxnID, _ = strconv.ParseInt(doc.Data()["value"].(string), 10, 64)
	}

	newGrayllTxnID := lastGrayllTxnID + 1
	var newGrayllTxnIdString string
	newGrayllTxnIdDecimalCount := recursionCountDigits(newGrayllTxnID)
	if newGrayllTxnIdDecimalCount < 18 {
		diff := 18 - newGrayllTxnIdDecimalCount
		var i int64
		for i = 1; i <= diff; i++ {
			newGrayllTxnIdString = "0" + newGrayllTxnIdString
		}
	}
	latestGrayllTxID := newGrayllTxnIdString + strconv.FormatInt(int64(newGrayllTxnID), 10)

	go func() {
		if _, err := client.Collection("Environment").Doc("jnMha28zlJc5cygik3cN").Set(ctx, map[string]interface{}{
			"key":   "GrayllTxnID",
			"value": latestGrayllTxID,
		}); err != nil {
			log.Printf("[ERROR] unable to update Environment Collection, error : %v", err)
		}
	}()

	return latestGrayllTxID, nil
}
