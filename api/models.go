package api

import (
	jwttool "bitbucket.org/grayll/grayll.io-grz-arkady/jwt-tool"
	//utils "bitbucket.org/grayll/grayll.io-utils"
	cloudtasks "cloud.google.com/go/cloudtasks/apiv2"
	"cloud.google.com/go/firestore"
	"github.com/algolia/algoliasearch-client-go/v3/algolia/search"
	"github.com/huyntsgs/stellar-service/assets"
)

type Config struct {
	ProjectId              string `json:"projectId"`
	QueueId                string `json:"queueId"`
	PerfQueueId            string `json:"perfQueueId"`
	LocationId             string `json:"locationId"`
	TaskUrl                string `json:"taskUrl"`
	PerformanceUrl         string `json:"performanceUrl"`
	PositionTopic          string `json:"positionTopic"`
	PositionTopicOpenClose string `json:"positionTopicOpenClose"`
	ServiceAccountEmail    string `json:"serviceAccountEmail"`

	HotWalletOne         string  `json:"hotWalletOne"`
	HotWalletOneSeed     string  `json:"hotWalletOneSeed"`
	HotWalletTwo         string  `json:"hotWalletTwo"`
	HotWalletTwoSeed     string  `json:"hotWalletTwoSeed"`
	IsMainNet            bool    `json:"isMainNet"`
	AssetCode            string  `json:"assetCode"`
	IssuerAddress        string  `json:"issuerAddress"`
	XlmLoanerSeed        string  `json:"xlmLoanerSeed"`
	XlmLoanerAddress     string  `json:"xlmLoanerAddress"`
	HotWalletAddress1    string  `json:"hotWalletAddress1"`
	RedisHost            string  `json:"redisHost"`
	RedisPort            int     `json:"redisPort"`
	RedisPass            string  `json:"redisPass"`
	HorizonUrl           string  `json:"horizonUrl"`
	Host                 string  `json:"host"`
	Numberify            string  `json:"numberify"`
	SuperAdminAddress    string  `json:"superAdminAddress"`
	SuperAdminSeed       string  `json:"superAdminSeed"`
	SellingPrice         float64 `json:"sellingPrice"`
	SellingPercent       int     `json:"sellingPercent"`
	ApiKey               string  `json:"apiKey"`
	RetryFailAlgoQueueId string  `json:"retryFailAlgoQueueId"`
	RetryFailAlgoUrl     string  `json:"retryFailAlgoUrl"`
}
type ApiContext struct {
	Store           *firestore.Client
	LiquidityClient *firestore.Client
	Jwt             *jwttool.JwtToolkit
	GrayllAppClient *firestore.Client
	Cache           *RedisCache
	CloudTaskClient *cloudtasks.Client
	Config          *Config
	Asset           assets.Asset
	algoliaIndex    *search.Index
	ProjectID       string
}
type (
	// user data params
	UserData struct {
		UserId        float64 `json:"user_id"`
		AlgorithmType string  `json:"algorithm_type"`
		Start         int     `json:"start"`
		Count         int     `json:"count"`
	}
	AlgoInputData struct {
		Action string            `json:"action"`
		Data   AlgorithmPosition `json:"data"`
	}
	AlgoCloseAllInputData struct {
		Action string              `json:"action"`
		Data   []AlgorithmPosition `json:"data"`
	}
	AlgoCloseAllParam struct {
		GrayllTxs []string `json:"grayllTxs"`
	}
	AlgorithmPosition struct {
		UserId               string  `json:"user_id"`
		RefererId            string  `json:"referer_id,omitempty"`
		OpenPositionTotalUSD float64 `json:"open_position_total_$,omitempty"`
		StellarTxId          string  `json:"open_stellar_transaction_id,omitempty"`

		GrayllTxId             string `json:"grayll_transaction_id,omitempty"`
		AlgorithmType          string `json:"algorithm_type,omitempty"`
		OpenPositionTimestamp  int64  `json:"open_position_timestamp,omitempty"`
		ClosePositionTimestamp int64  `json:"close_position_timestamp,omitempty"`

		Duration       int64  `json:"duration"`
		DurationString string `json:"duration_string"`
		Status         string `json:"status"`
		Action         string `json:"action"`

		CurrentValueGRX              float64 `json:"current_value_GRX,omitempty"`
		CurrentValueGRZ              float64 `json:"current_value_GRZ,omitempty"`
		CurrentValueGRY              float64 `json:"current_value_GRY,omitempty"`
		CurrentPositionValueUSD      float64 `json:"current_position_value_$,omitempty"`
		CurrentPositionValueGRX      float64 `json:"current_position_value_GRX,omitempty"`
		CurrentPositionROIUSD        float64 `json:"current_position_ROI_$"`
		CurrentPositionROIPercentage float64 `json:"current_position_ROI_percent"`

		OpenValueGRX         float64 `json:"open_value_GRX,omitempty"`
		OpenValueGRZ         float64 `json:"open_value_GRZ,omitempty"`
		OpenValueGRY         float64 `json:"open_value_GRY,omitempty"`
		OpenPositionFeeUSD   float64 `json:"open_position_fee_$,omitempty"`
		OpenPositionFeeGRX   float64 `json:"open_position_fee_GRX,omitempty"`
		OpenPositionValueUSD float64 `json:"open_position_value_$,omitempty"`
		OpenPositionTotalGRX float64 `json:"open_position_total_GRX,omitempty"`
		OpenPositionValueGRZ float64 `json:"open_position_value_GRZ,omitempty"`
		OpenPositionValueGRX float64 `json:"open_position_value_GRX,omitempty"`

		CloseValueGRX         float64 `json:"close_value_GRX"`
		CloseValueGRZ         float64 `json:"close_value_GRZ"`
		ClosePositionTotalUSD float64 `json:"close_position_total_$"`
		ClosePositionValueUSD float64 `json:"close_position_value_$"`

		ClosePositionTotalGRX float64 `json:"close_position_total_GRX"`
		ClosePositionValueGRX float64 `json:"close_position_value_GRX"`
		ClosePositionTotalGRZ float64 `json:"close_position_total_GRZ"`
		//ClosePositionValueGRZ float64 `json:"close_position_value_GRZ"`

		ClosePositionROIUSD            float64 `json:"close_position_ROI_$"`
		ClosePositionROIPercentage     float64 `json:"close_position_ROI_percent"`
		Close_position_ROI_percent_NET float64 `json:"close_position_ROI_percent_NET"`
		ClosePositionFeeGRX            float64 `json:"close_position_fee_GRX"`
		ClosePositionFeeUSD            float64 `json:"close_position_fee_$"`
		ClosePerformanceFeeUSD         float64 `json:"close_performance_fee_$"`
		ClosePerformanceFeeGRX         float64 `json:"close_performance_fee_GRX"`
		ClosePerfFeeRefererGRX         float64 `json:"close_perf_fee_referer_GRX,omitempty"`
	}

	AlgorithmPositionOpen struct {
		UserId               string  `json:"user_id"`
		OpenPositionTotalUSD float64 `json:"open_position_total_$"`
		StellarTxId          string  `json:"open_stellar_transaction_id,omitempty"`

		GrayllTxId            string `json:"grayll_transaction_id,omitempty"`
		AlgorithmType         string `json:"algorithm_type"`
		OpenPositionTimestamp int64  `json:"open_position_timestamp"`

		GrxPrice float64 `json:"grx_price_$"`
		GrzPrice float64 `json:"grz_price_$"`

		Duration                     int64   `json:"duration"`
		Status                       string  `json:"status"`
		Action                       string  `json:"action, omitempty"`
		CurrentValueGRX              float64 `json:"current_value_GRX"`
		CurrentValueGRZ              float64 `json:"current_value_GRZ"`
		CurrentPositionValueUSD      float64 `json:"current_position_value_$"`
		CurrentPositionValueGRX      float64 `json:"current_position_value_GRX"`
		CurrentPositionROIUSD        float64 `json:"current_position_ROI_$"`
		CurrentPositionROIPercentage float64 `json:"current_position_ROI_%"`
		Info                         string  `json:"info,omitempty"`

		OpenPositionFeeUSD   float64 `json:"open_position_fee_$"`
		OpenPositionFeeGRX   float64 `json:"open_position_fee_GRX"`
		OpenPositionValueUSD float64 `json:"open_position_value_$"`
		OpenPositionTotalGRX float64 `json:"open_position_total_GRX"`
		OpenPositionValueGRZ float64 `json:"open_position_value_GRZ"`
		OpenPositionValueGRX float64 `json:"open_position_value_GRX"`
		OpenValueGRX         float64 `json:"open_value_GRX"`
		OpenValueGRZ         float64 `json:"open_value_GRZ"`
	}

	AlgorithmPositionClose struct {
		UserId                string  `json:"user_id"`
		ClosePositionTotalUSD float64 `json:"close_position_total_$,omitempty"`
		StellarTxId           string  `json:"close_stellar_transaction_id,omitempty"`
		GrayllTxId            string  `json:"grayll_transaction_id"`

		AlgorithmType          string `json:"algorithm_type"`
		ClosePositionTimestamp int64  `json:"close_position_timestamp,omitempty"`

		GrxPrice float64 `json:"grx_price_$"`
		GrzPrice float64 `json:"grz_price_$"`

		Status                     string  `json:"status"`
		Action                     string  `json:"action, omitempty"`
		Duration                   int64   `json:"duration,omitempty"`
		CloseValueGRX              float64 `json:"close_value_GRX,omitempty"`
		CloseValueGRZ              float64 `json:"close_value_GRZ,omitempty"`
		ClosePositionValueUSD      float64 `json:"close_position_value_$,omitempty"`
		ClosePositionValueGRX      float64 `json:"close_position_value_GRX,omitempty"`
		ClosePositionROIUSD        float64 `json:"close_position_ROI_$,omitempty"`
		ClosePositionROIPercentage float64 `json:"close_position_ROI_%,omitempty"`
		Info                       string  `json:"info,omitempty"`

		ClosePositionTotalGRX  float64 `json:"close_position_total_GRX,omitempty"`
		ClosePositionValueGRZ  float64 `json:"close_position_value_GRZ,omitempty"`
		ClosePositionFeeUSD    float64 `json:"close_position_fee_$,omitempty"`
		ClosePerformanceFeeUSD float64 `json:"close_performance_fee_$,omitempty"`
		ClosePerformanceFeeGRX float64 `json:"close_performance_fee_GRX,omitempty"`
	}

	PositionUpdate struct {
		UserId                       string  `json:"user_id"`
		GrayllTxId                   string  `json:"grayll_transaction_id"`
		CurrentValueGRX              float64 `json:"current_value_GRX"`
		CurrentValueGRZ              float64 `json:"current_value_GRZ"`
		CurrentPositionValueUSD      float64 `json:"current_position_value_$"`
		CurrentPositionValueGRX      float64 `json:"current_position_value_GRX"`
		CurrentPositionROIUSD        float64 `json:"current_position_ROI_$"`
		CurrentPositionROIPercentage float64 `json:"current_position_ROI_%"`
	}
)
