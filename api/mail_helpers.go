package api

import (
	"fmt"
	"time"
)

func GenOpenPositionGRZ(position AlgorithmPosition) (string, string, string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Opened`, position.AlgorithmType)

	contents := []string{
		time.Unix(position.OpenPositionTimestamp, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`You have opened a new %s | Arkady algorithmic position. `, position.AlgorithmType),

		fmt.Sprintf(`Open Algo Position Value: $%v. `, position.OpenPositionValueUSD),

		fmt.Sprintf(`GRZ Rate | $%v.`, position.OpenValueGRZ),

		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | $%v. `, position.AlgorithmType, position.OpenPositionFeeUSD),

		fmt.Sprintf(`GRX Rate | $%v.`, position.OpenValueGRX),

		fmt.Sprintf(`GRX | Total Algo Position Value | %v GRX. `, position.OpenPositionTotalGRX),

		fmt.Sprintf(`%s | Algo Management Fee @ 0.3%% | %v GRX. `, position.AlgorithmType, position.OpenPositionFeeGRX),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s. `, position.GrayllTxId),
	}
	content := ""
	for i, sent := range contents {
		content = content + sent
		if i == 0 {
			content = content + ". "
		}
	}

	url := "Transaction ID | https://stellar.expert/explorer/public/search?term=" + position.StellarTxId
	//log.Println(content)
	contents = append(contents, url)

	return title, content, url, contents
}

func GenOpenPositionGRY(position AlgorithmPosition) (string, []string) {

	title := fmt.Sprintf(`GRAYLL | %s Algo Position Opened`, position.AlgorithmType)

	content := []string{
		time.Unix(position.OpenPositionTimestamp, 0).Format(`15:04 | 02-01-2006`),

		fmt.Sprintf(`You have opened a new %s | Balthazar algorithmic position.`, position.AlgorithmType),

		fmt.Sprintf(`Open Algo Position Value: $%f
		GRY Rate | $%f 
		%s | Algo Management Fee @ 1.8%% | $%f`, position.OpenPositionValueUSD, position.OpenValueGRZ, position.AlgorithmType, position.OpenPositionFeeUSD),

		fmt.Sprintf(`GRX Rate | $%f 
		GRX | Total Algo Position Value | %f GRX
		%s | Algo Management Fee @ 1.8%% | %f GRX`, position.OpenValueGRX, position.OpenPositionTotalGRX, position.AlgorithmType, position.OpenPositionFeeGRX),

		fmt.Sprintf(`GRAYLL | Transaction ID | %s
		Stellar | Transaction ID %s`, position.GrayllTxId, position.StellarTxId),
	}

	return title, content
}
