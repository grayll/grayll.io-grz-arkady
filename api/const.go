package api

const (
	DEFAULT_LIMIT  = 100
	DEFAULT_OFFSET = 0
)

// Define all error codes send back to clients
// This helps debug easier
const (
	UNKNOWN_ERROR          = "UNKNOWN_ERROR"
	TOKEN_EXPIRED          = "TOKEN_EXPIRED"
	INTERNAL_ERROR         = "INTERNAL_ERROR"
	NO_TOKEN               = "NO_TOKEN"
	TOKEN_INVALID          = "TOKEN_INVALID"
	INVALID_PARAMS         = "INVALID_PARAMS"
	DUPLICATE_NAME         = "DUPLICATE_NAME"
	INVALID_CODE           = "INVALID_CODE"
	EMAIL_IN_USED          = "EMAIL_IN_USED"
	INVALID_UNAME_PASSWORD = "INVALID_UNAME_PASSWORD"
	UNVERIFIED             = "UNVERIFIED"
	IP_CONFIRM             = "IP_CONFIRM"
	EMAIL_NOT_EXIST        = "EMAIL_NOT_EXIST"
	EMAIL_VERIFIED         = "EMAIL_VERIFIED"
	ADDRESS_EXIST          = "ADDRESS_EXIST"
	INVALID_ADDRESS        = "INVALID_ADDRESS"
	SUCCESS                = "SUCCESS"
	PHONE_EXIST            = "PHONE_EXIST"
	TX_FAIL                = "TX_FAIL"
	PRICE_LOWER_LIMIT      = "PRICE_LOWER_LIMIT"
	TX_IN_USED             = "TX_IN_USED"
	INTERNAL_ERROR_RETRY   = "INTERNAL_ERROR_RETRY"
)

const (
	API_KEY = "AIzaSyBoGmsWYP-CsATX8c1sW8AAc4ua4bl3_SY"
	API_URL = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/%s?key=AIzaSyBoGmsWYP-CsATX8c1sW8AAc4ua4bl3_SY"
)
