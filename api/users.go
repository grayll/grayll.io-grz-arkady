package api

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"strings"

	"github.com/go-redis/redis"

	//"io/ioutil"
	"reflect"
	"strconv"

	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/grayll/grayll.io-grz-arkady/mail"
	cloudtasks "cloud.google.com/go/cloudtasks/apiv2"
	"cloud.google.com/go/firestore"
	"cloud.google.com/go/pubsub"
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/ptypes/timestamp"
	stellar "github.com/huyntsgs/stellar-service"
	"google.golang.org/api/iterator"
	taskspb "google.golang.org/genproto/googleapis/cloud/tasks/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	ConfirmRegistrationSub = "Please Confirm Your GRAYLL App Registration Request"
	ConfirmIpSub           = "GRAYLL | New IP Address Verification"
	LoginSuccess           = "GRAYLL | Account Login Successful"
	ResetPasswordSub       = "GRAYLL | Reset Password Verification"
	ChangeEmailSub         = "GRAYLL | Confirm change email request"
	RevealSecretTokenSub   = "GRAYLL | Reveal Secret Key"

	VerifyEmail       = "verifyEmail"
	ResetPassword     = "resetPassword"
	ChangeEmail       = "changeEmail"
	ConfirmIp         = "confirmIp"
	UID               = "Uid"
	RevealSecretToken = "revealSecretToken"
	TokeExpiredTime   = 24*60*60 - 2
	//TokeExpiredTime = 8 * 60
)

type UserHandler struct {
	apiContext   *ApiContext
	PubSubClient *pubsub.Client
	QueueNumber  int
}

// Creates new UserHandler.
// UserHandler accepts interface UserStore.
// Any data store implements UserStore could be the input of the handle.
func NewUserHandler(apiContext *ApiContext) UserHandler {
	return UserHandler{apiContext: apiContext}
}

// Login handles login router.
// Function validates parameters and call Login from UserStore.
func (h UserHandler) OpenPostion() gin.HandlerFunc {
	return func(c *gin.Context) {
		var params AlgorithmPosition
		//var input AlgoInputData
		ctx := context.Background()
		err := c.BindJSON(&params)
		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ open position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position invalid params")
			return
		}
		//log.Println("params:", params)
		if err = validateOpenPositionRequestBody(params); err != nil {
			log.Printf("[ERROR] validateOpenPositionRequestBody error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position invalid params")
			return
		}

		uid := c.GetString("Uid")
		if uid != params.UserId {
			log.Printf("[ERROR] uid in token: %s is not the uid in open position request: %s\n", uid, params.UserId)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position invalid params user_id")
			return
		}
		log.Println(uid, "open position:", params)
		// Check open stellar txhash
		openStellarTxId := params.StellarTxId
		_, err = h.apiContext.LiquidityClient.Doc("open_stellar_txs/" + openStellarTxId).Get(ctx)

		if err != nil && grpc.Code(err) != codes.NotFound {
			msg := fmt.Sprintf("[HACK] %s transaction already used - %s", uid, openStellarTxId, err)
			docRef := h.apiContext.Store.Collection("hacks/" + uid + "/txs_in_used").NewDoc()
			docRef.Set(ctx, map[string]interface{}{
				"msg":  msg,
				"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
			})
			GinRespond(c, http.StatusOK, TX_IN_USED, "Open algo position internal error")
			log.Println("[ERROR]", uid, "open position:", msg)
			return
		} else if err != nil && strings.Contains(err.Error(), "closing") {
			// retry connect
			log.Println("[ERROR]", uid, "open position con closing", err)
			h.apiContext.LiquidityClient, err = ReconnectFireStore("grayll-liquidity", 60)
			if err != nil {
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}
			_, err = h.apiContext.LiquidityClient.Doc("open_stellar_txs/" + openStellarTxId).Get(ctx)
			if err != nil && grpc.Code(err) != codes.NotFound {
				msg := fmt.Sprintf("[HACK] %s transaction already used 1 - %s", uid, openStellarTxId)
				docRef := h.apiContext.Store.Collection("hacks/" + uid + "/txs_in_used").NewDoc()
				docRef.Set(ctx, map[string]interface{}{
					"msg":  msg,
					"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
				})
				GinRespond(c, http.StatusOK, TX_IN_USED, "Open algo position internal error")
				log.Println("[ERROR]", uid, "open position checking hack:", msg)
				return
			}
		}

		grayllTxId, err := h.apiContext.Cache.GetNewGrayllTxId()
		if err != nil {
			log.Println("[ERROR]", uid, "open position:", " can not generate grayllTxId from redis")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR_RETRY, "Can not parse tx hash information")
			return
		}

		// Validate open stellar tx id
		//url := h.apiContext.Config.HorizonUrl + fmt.Sprintf("ledgers/%s/payments", params.StellarTxId)
		userAddress, err := h.apiContext.Cache.GetPublicKeyFromUid(uid)
		if err != nil {
			log.Println("[ERROR] ", uid, "open position:", " can not get user public key from redis")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR_RETRY, "Can not get user public key information")
			return
		}

		payments, err := ParsePaymentFromTxHash(params.StellarTxId, stellar.GetHorizonClient())
		if err != nil {
			time.Sleep(time.Second * 1)
			for i := 0; i < 60; i++ {
				payments, err = ParsePaymentFromTxHash(params.StellarTxId, stellar.GetHorizonClient())
				if len(payments) > 0 {
					break
				}
			}
		}
		hasPayment := false
		grzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			log.Println("[ERROR] ", uid, "open position:", " can not get grxusd from redis")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR_RETRY, "Can not get grzusd from redis")
		}
		grxusd, err := h.apiContext.Cache.GetGRXUsd()
		if err != nil {
			log.Println("[ERROR] ", uid, "open position:", " can not get grxusd from redis")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR_RETRY, "Can not get grxusd from redis")
		}

		//log.Println("Payments:", c.ClientIP(), payments)
		for _, payment := range payments {
			amount, _ := strconv.ParseFloat(payment.Amount, 64)
			if payment.From == userAddress && payment.To == h.apiContext.Config.HotWalletOne && amount >= params.OpenPositionValueGRX {
				hasPayment = true
				params.StellarTxId = payment.GetID()
				break
			} else if payment.From == userAddress && payment.To == h.apiContext.Config.HotWalletOne && amount < params.OpenPositionValueGRX {
				// hack
				log.Println("[HACK] ", uid, "open position - ", "amount < params.OpenPositionValueGRX")
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position internal error")
				return
			}
		}
		if !hasPayment {
			log.Println("[ERROR] ", uid, "open position:", "can not get payment infor from txhash", openStellarTxId)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR_RETRY, "can not get payment infor from txhash")
			return
		}

		//batch := h.apiContext.Store.Batch()
		grayllTxIdNumber, _ := strconv.Atoi(grayllTxId)
		params.GrayllTxId = fmt.Sprintf("%018s", grayllTxId)

		params.UserId = uid

		params.OpenPositionTimestamp = time.Now().Unix()

		params.OpenValueGRX = grxusd
		params.OpenValueGRZ = grzusd

		params.Duration = 0
		params.DurationString = "00:00 | 000"
		params.Status = "OPEN"
		params.Action = "OPEN"
		params.CurrentValueGRX = params.OpenValueGRX
		params.CurrentValueGRZ = params.OpenValueGRZ

		params.CurrentPositionValueUSD = params.OpenPositionValueUSD
		params.CurrentPositionValueGRX = params.OpenPositionValueGRX

		params.CurrentPositionROIUSD = params.OpenPositionValueUSD - params.OpenPositionTotalUSD
		params.CurrentPositionROIPercentage = params.CurrentPositionROIUSD * 100 / params.OpenPositionTotalUSD

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", params.UserId, params.GrayllTxId)
		//docRef1 := h.apiContext.Store.Doc(docPath)

		_params, err := json.Marshal(params)
		if err != nil {
			log.Printf("[ERROR] unable to marshal _grzPosition data, error : %v", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}
		//log.Println("_params:", string(_params))
		positionMap := make(map[string]interface{})
		err = json.Unmarshal(_params, &positionMap)
		if err != nil {
			log.Printf("[ERROR] unable to Unmarshal grzPosition data, error : %v", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}

		_, err = h.apiContext.Store.Doc(docPath).Create(ctx, positionMap)
		if err != nil {
			log.Printf("[ERROR] openposition can not save firestore", uid, err)
			h.apiContext.Store, err = ReconnectFireStore(h.apiContext.ProjectID, 60)
			if err != nil {
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}

			_, err = h.apiContext.Store.Doc(docPath).Create(ctx, positionMap)
			if err != nil {
				log.Printf("[ERROR] openposition can not save firestore after reconnect", uid, err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}
		}
		// Store tx hash in grayll-liquidity
		h.apiContext.LiquidityClient.Doc("open_stellar_txs/"+openStellarTxId).Set(ctx, map[string]interface{}{
			"user_id": uid,
		})

		// Push Pub/Sub server

		//PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)
		err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)
		if err != nil {
			log.Println("[ERROR] openposition unable publish msg", uid, err)
			h.PubSubClient, _ = ReconnectPubsub(h.apiContext.ProjectID, 60)
			err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)
			if err != nil {
				log.Println("[ERROR] openposition unable publish msg after retry", uid, err)
			}
		}
		subQueueId := "-" + strconv.Itoa(grayllTxIdNumber%h.QueueNumber)
		data := map[string]interface{}{
			"user_id":                 uid,
			"grayll_transaction_id":   params.GrayllTxId,
			"open_position_value_$":   params.OpenPositionValueUSD,
			"open_value_GRZ":          params.OpenValueGRZ,
			"open_position_timestamp": params.OpenPositionTimestamp,
			"algorithm_type":          params.AlgorithmType,
			"sub_queue_id":            subQueueId,
		}
		// This will run every one minute
		//h.ScheduleTask(data, time.Now().Unix()+60, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
		_, err = h.ScheduleTask(data, time.Now().Unix()+60, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
		if err != nil {
			h.apiContext.CloudTaskClient, err = ReconnectCloudTask(h.apiContext.ProjectID, 60)
			_, err = h.ScheduleTask(data, time.Now().Unix()+60, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
			if err != nil {
				log.Println("[ERROR] Reconnect cloud task client but failed", uid, err)
			}
		}
		// Schedule performance update every 24 hours
		h.ScheduleTask(data, time.Now().Unix()+24*60*60, h.apiContext.Config.PerformanceUrl, h.apiContext.Config.PerfQueueId)

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS, "grayllTxId": params.GrayllTxId, "stellarTxId": params.StellarTxId,
		})
	}
}

func (h UserHandler) ReTryFailAlgo() gin.HandlerFunc {
	return func(c *gin.Context) {
		var params AlgorithmPosition
		//var input AlgoInputData
		ctx := context.Background()
		err := c.BindJSON(&params)
		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ open position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position invalid params")
			return
		}
		log.Println("params:", params)
		if err = validateOpenPositionRequestBody(params); err != nil {
			log.Printf("[ERROR] validateOpenPositionRequestBody error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Open algo position invalid params")
			return
		}

		uid := params.UserId
		// Check open stellar txhash
		openStellarTxId := params.StellarTxId
		txDoc, err := h.apiContext.LiquidityClient.Doc("open_stellar_txs/" + openStellarTxId).Get(ctx)
		if err != nil && grpc.Code(err) != codes.NotFound {
			log.Println("ReTryFailAlgo-Data:already used:", txDoc.Data())
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}

		grayllTxId, err := h.apiContext.Cache.GetNewGrayllTxId()
		if err != nil {
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}

		grayllTxIdNumber, _ := strconv.Atoi(grayllTxId)

		// Validate open stellar tx id
		//url := h.apiContext.Config.HorizonUrl + fmt.Sprintf("ledgers/%s/payments", params.StellarTxId)
		userAddress, err := h.apiContext.Cache.GetPublicKeyFromUid(uid)
		if err != nil {
			log.Printf("Can not find user address - %s in cache - error %v \n", uid, err)
			GinRespond(c, http.StatusOK, INVALID_CODE, "Can not parse ledger information")
			return
		}

		log.Println("OpenPostion-StellarTxId:", c.ClientIP(), params.StellarTxId)
		payments, err := ParsePaymentFromTxHash(params.StellarTxId, stellar.GetHorizonClient())
		if err != nil {
			time.Sleep(time.Second * 1)
			for i := 0; i < 180; i++ {
				payments, err = ParsePaymentFromTxHash(params.StellarTxId, stellar.GetHorizonClient())
				if len(payments) > 0 {
					break
				}
			}
		}
		hasPayment := false
		//log.Println("Payments:", c.ClientIP(), payments)
		for _, payment := range payments {
			amount, _ := strconv.ParseFloat(payment.Amount, 64)
			if payment.From == userAddress && payment.To == h.apiContext.Config.HotWalletOne && amount >= params.OpenPositionValueGRX {
				hasPayment = true
				params.StellarTxId = payment.GetID()
				break
			}
		}
		if !hasPayment {
			GinRespond(c, http.StatusBadRequest, INVALID_CODE, "Can not parse tx hash information")
			return
		}

		grzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			// Need to store tx id and verify later
			params.UserId = uid
			params.OpenPositionTimestamp = time.Now().Unix()

			_params, err := json.Marshal(params)
			if err != nil {
				log.Printf("[ERROR]-OpenPostion unable to marshal params data, error : %v", err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}
			//log.Println("_params:", string(_params))
			positionMap := make(map[string]interface{})
			err = json.Unmarshal(_params, &positionMap)
			if err != nil {
				log.Printf("[ERROR]-OpenPostion unable to Unmarshal positionMap, error : %v", err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}

			positionMap["open_stellar_transaction_id"] = openStellarTxId
			h.apiContext.Store.Doc("failed_trans/"+openStellarTxId).Create(ctx, positionMap)
			h.ScheduleTask(positionMap, time.Now().Unix()+60, h.apiContext.Config.RetryFailAlgoUrl, h.apiContext.Config.RetryFailAlgoQueueId)

			GinRespond(c, http.StatusOK, INVALID_CODE, "Can not parse tx hash information")
			return
		}
		grxusd, err := h.apiContext.Cache.GetGRXUsd()

		batch := h.apiContext.Store.Batch()
		params.GrayllTxId = fmt.Sprintf("%018s", grayllTxId)

		params.UserId = uid
		params.OpenPositionTimestamp = time.Now().Unix()

		params.OpenValueGRX = grxusd
		params.OpenValueGRZ = grzusd

		params.Duration = 0
		params.DurationString = "00:00 | 000"
		params.Status = "OPEN"
		params.Action = "OPEN"
		params.CurrentValueGRX = params.OpenValueGRX
		params.CurrentValueGRZ = params.OpenValueGRZ

		params.CurrentPositionValueUSD = params.OpenPositionValueUSD
		params.CurrentPositionValueGRX = params.OpenPositionValueGRX

		params.CurrentPositionROIUSD = params.OpenPositionValueUSD - params.OpenPositionTotalUSD
		params.CurrentPositionROIPercentage = params.CurrentPositionROIUSD * 100 / params.OpenPositionTotalUSD

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", params.UserId, params.GrayllTxId)
		docRef1 := h.apiContext.Store.Doc(docPath)

		_params, err := json.Marshal(params)
		if err != nil {
			log.Printf("[ERROR] unable to marshal _grzPosition data, error : %v", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}
		//log.Println("_params:", string(_params))
		positionMap := make(map[string]interface{})
		err = json.Unmarshal(_params, &positionMap)
		if err != nil {
			log.Printf("[ERROR] unable to Unmarshal grzPosition data, error : %v", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}

		batch.Create(docRef1, positionMap)
		_, err = batch.Commit(ctx)
		if err != nil {
			log.Printf("[ERROR] unable to add open position data into firestore,"+
				" batch commit, error : %v - grayllId: %v", err, grayllTxId)
			grayllTxId, err := h.apiContext.Cache.GetNewGrayllTxId()
			if err != nil {
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}
			positionMap["grayll_transaction_id"] = grayllTxId
			batch.Create(docRef1, positionMap)
			_, err = batch.Commit(ctx)
			if err != nil {
				log.Printf("[ERROR] unable to add open position data into firestore 1,"+
					" batch commit, error : %v - grayllId: %v", err, grayllTxId)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Close algo position internal error")
				return
			}
		}
		// Store tx hash in grayll-liquidity
		h.apiContext.LiquidityClient.Doc("open_stellar_txs/"+openStellarTxId).Set(ctx, map[string]interface{}{
			"user_id": uid,
		})

		// Push Pub/Sub server
		PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)

		subQueueId := "-" + strconv.Itoa(grayllTxIdNumber%h.QueueNumber)
		data := map[string]interface{}{
			"user_id":                 uid,
			"grayll_transaction_id":   params.GrayllTxId,
			"open_position_value_$":   params.OpenPositionValueUSD,
			"open_value_GRZ":          params.OpenValueGRZ,
			"open_position_timestamp": params.OpenPositionTimestamp,
			"algorithm_type":          params.AlgorithmType,
			"sub_queue_id":            subQueueId,
		}
		// This will run every one minute
		h.ScheduleTask(data, time.Now().Unix()+60, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
		// Schedule performance update every 12 hours
		h.ScheduleTask(data, time.Now().Unix()+24*60*60, h.apiContext.Config.PerformanceUrl, h.apiContext.Config.PerfQueueId)

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS, "grayllTxId": params.GrayllTxId, "stellarTxId": params.StellarTxId,
		})
	}
}

func (h UserHandler) ClosePosition() gin.HandlerFunc {
	return func(c *gin.Context) {
		var params AlgorithmPosition
		ctx := context.Background()

		err := c.BindJSON(&params)
		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ open position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
			return
		}

		uid := c.GetString(UID)
		prc, err := h.apiContext.Cache.GetTxProcess(uid, params.GrayllTxId)
		if err != nil && err.Error() != "redis: nil" {
			log.Println("[ERROR] unable get txprocess infor", uid, err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Can not get tx infor")
			return
		}

		if prc == "BUSY" {
			msg := fmt.Sprintf("[HACK] transaction is busy %s - %s", uid, params.GrayllTxId)
			docRef := h.apiContext.Store.Collection("hacks/" + uid + "/closed").NewDoc()
			docRef.Set(ctx, map[string]interface{}{
				"msg":  msg,
				"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
			})
			log.Println(msg)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "")
			return
		}
		h.apiContext.Cache.SetTxProcess(uid, params.GrayllTxId)

		// Check whether blocking close position
		pauseUntil, err := h.apiContext.Cache.Client.HGet("pauseClosing", "pauseUntil").Int64()
		log.Println("pauseUntil, err:", pauseUntil, err)
		if err == redis.Nil {
			pauseUntil = 0
		}
		pauseClosing, err := h.apiContext.Cache.Client.HGet("pauseClosing", "pauseClosing").Int()
		log.Println("pauseClosing, err:", pauseClosing, err)
		if err == redis.Nil {
			pauseClosing = 0
		}
		log.Println("pauseClosing, pauseUntil:", pauseClosing, pauseUntil)

		if pauseClosing == 1 || pauseUntil > time.Now().Unix() {
			log.Println("HACK user is trying to closing position", uid, params.GrayllTxId)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "")
			go func() {
				content := fmt.Sprintf("User %s is trying close %s with grayll transaction id: %s", uid, "Grz Arkardy", params.GrayllTxId)
				err := mail.SendNoticeMail("grayll@grayll.io", "Grayll", "Closing algo during GRX market volatile", []string{content})
				if err != nil {
					log.Println("[ERROR]- pause closing send mail:", err)
				}
			}()
			return
		}

		// Get position from database
		position, err := h.apiContext.Store.Doc("algo_positions/users/" + uid + "/" + params.GrayllTxId).Get(ctx)
		if err != nil {
			log.Printf("[ERROR] unable to get position with id : %s - err: %v", uid, params.GrayllTxId, err)
			h.apiContext.Store, _ = ReconnectFireStore(h.apiContext.ProjectID, 60)
			position, err = h.apiContext.Store.Doc("algo_positions/users/" + uid + "/" + params.GrayllTxId).Get(ctx)
			if err != nil {
				log.Printf("[ERROR] unable to get position with after retry : %s - err: %v", uid, params.GrayllTxId, err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}
		}

		err = MapToStruct(&params, position.Data())
		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRY open position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
			return
		}
		if params.Status != "OPEN" {
			msg := fmt.Sprintf("[HACK] position already closed %s - %s", uid, params.GrayllTxId)
			docRef := h.apiContext.Store.Collection("hacks/" + uid + "/closed").NewDoc()
			docRef.Set(ctx, map[string]interface{}{
				"msg":  msg,
				"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
			})
			log.Println(msg)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "closed")
			return
		}
		//log.Println("params:", params)
		grzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			log.Println("[ERROR] close position - unable get grzusd from cache")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "can not get grzusd")
			return
		}
		grxusd, err := h.apiContext.Cache.GetGRXUsd()
		if err != nil {
			log.Println("[ERROR] close position - unable get grxusd from cache")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "can not get grxusd")
			return
		}
		params.CloseValueGRX = grxusd
		params.CloseValueGRZ = grzusd
		// let close_position_total_$ = position.open_position_value_$ * (((grzusd - position.open_value_GRZ)/position.open_value_GRZ) + 1)
		params.ClosePositionTotalUSD = params.OpenPositionValueUSD * (((grzusd - params.OpenValueGRZ) / params.OpenValueGRZ) + 1)
		params.ClosePositionFeeUSD = params.ClosePositionTotalUSD * 0.003
		params.ClosePositionFeeGRX = params.ClosePositionFeeUSD / grxusd

		params.ClosePositionROIUSD = params.ClosePositionTotalUSD - params.OpenPositionValueUSD
		params.ClosePerformanceFeeUSD = 0
		netRoi := params.ClosePositionROIUSD - params.ClosePositionFeeUSD
		// if netRoi <= 0 {
		// 	netRoi = 2
		// }
		if netRoi > 0 {
			refererUid, _ := h.apiContext.Cache.GetRefererUid(uid)
			fee := 0.18
			if refererUid != "" {
				fee = 0.15
			}
			params.ClosePerformanceFeeUSD = netRoi * fee
			params.ClosePerformanceFeeGRX = params.ClosePerformanceFeeUSD / grxusd
			if refererUid != "" {
				perfFeeReferer := netRoi * 0.03 / grxusd
				params.ClosePerfFeeRefererGRX, _ = strconv.ParseFloat(fmt.Sprintf("%.7f", perfFeeReferer), 64)
				params.RefererId = refererUid
			}
		}

		//     let close_position_total_GRX = close_position_total_$/grxusd
		params.ClosePositionTotalGRX = params.ClosePositionTotalUSD / grxusd
		//     let close_position_value_$ = close_position_total_$ - close_position_fee_$ - close_performance_fee_$
		params.ClosePositionValueUSD = params.ClosePositionTotalUSD - params.ClosePositionFeeUSD - params.ClosePerformanceFeeUSD
		params.ClosePositionValueGRX = params.ClosePositionValueUSD / grxusd
		//log.Println("ClosePositionValueGRX:", params.ClosePositionValueGRX)
		//     let close_position_ROI_percent = (grzusd - position.open_value_GRZ)*100/position.open_value_GRZ
		params.ClosePositionROIPercentage = (grzusd - params.OpenValueGRZ) * 100 / params.OpenValueGRZ
		//     let close_position_ROI_percent_NET = ((close_position_value_$-position.open_position_value_$)*100)/position.open_position_value_$
		params.Close_position_ROI_percent_NET = ((params.ClosePositionValueUSD - params.OpenPositionValueUSD) * 100) / params.OpenPositionValueUSD

		//     let close_position_ROI_$ = close_position_total_$ - position.open_position_value_$

		params.UserId = uid
		params.ClosePositionTimestamp = time.Now().Unix()
		params.Status = "CLOSED"
		params.Action = "CLOSED"
		params.DurationString = ConvertSectoDay(time.Now().Unix() - params.OpenPositionTimestamp)

		_params, err := json.Marshal(params)
		if err != nil {
			log.Printf("[ERROR] unable to marshal _grzPosition data, error : %v", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
			return
		}

		positionMap := make(map[string]interface{})
		err = json.Unmarshal(_params, &positionMap)

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", params.UserId, params.GrayllTxId)
		_, err = h.apiContext.Store.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
		if err != nil {
			log.Printf("[ERROR] closeposition unable to save position with id : %s - err: %v", uid, params.GrayllTxId, err)
			h.apiContext.Store, _ = ReconnectFireStore(h.apiContext.ProjectID, 60)
			_, err = h.apiContext.Store.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
			if err != nil {
				log.Printf("[ERROR] closeposition unable to save position with after retry : %s - err: %v", uid, params.GrayllTxId, err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}

		}
		h.apiContext.Cache.DelTxProcess(uid, params.GrayllTxId)

		err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)
		if err != nil {
			log.Println("[ERROR] closeposition - Can not publish message", uid, err)
			h.PubSubClient, _ = ReconnectPubsub(h.apiContext.ProjectID, 60)
			err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _params)
			if err != nil {
				log.Println("[ERROR] closeposition - Can not publish message after retry", uid, err)
			}
		}

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS,
		})

	}
}

func (h UserHandler) CloseAllPosition() gin.HandlerFunc {
	return func(c *gin.Context) {
		input := new(AlgoCloseAllParam)
		ctx := context.Background()
		err := c.BindJSON(&input)
		if err != nil {
			log.Println("[ERROR] Marshal all AlgorithmPosition: ", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Marshal all AlgorithmPosition error")
			return
		}
		uid := c.GetString(UID)
		// Check whether blocking close position
		pauseUntil, err := h.apiContext.Cache.Client.HGet("pauseClosing", "pauseUntil").Int64()
		log.Println("pauseUntil, err:", pauseUntil, err)
		if err == redis.Nil {
			pauseUntil = 0
		}
		pauseClosing, err := h.apiContext.Cache.Client.HGet("pauseClosing", "pauseClosing").Int()
		log.Println("pauseClosing, err:", pauseClosing, err)
		if err == redis.Nil {
			pauseClosing = 0
		}
		log.Println("pauseClosing, pauseUntil:", pauseClosing, pauseUntil)

		if pauseClosing == 1 || pauseUntil > time.Now().Unix() {
			log.Println("HACK user is trying to closing all position", uid)

			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "")
			go func() {
				content := fmt.Sprintf("User %s is trying close all %s", uid, "Grz Arkardy")
				err := mail.SendNoticeMail("grayll@grayll.io", "Grayll", "Closing algo during GRX market volatile", []string{content})
				if err != nil {
					log.Println("[ERROR]- pause closing send mail:", err)

					return
				}
			}()
			return
		}

		grxusd, err := h.apiContext.Cache.GetGRXUsd()
		if err != nil {
			log.Println("[ERROR] can not get current grxusd from cache")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Get update_price error")
		}
		grzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			log.Println("[ERROR] can not get current grxusd from cache")
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Get update_price error")
		}

		closeAllAlgo := AlgoCloseAllInputData{}
		for _, grayllTx := range input.GrayllTxs {

			//preventing submit close position in milisecond
			prc, err := h.apiContext.Cache.GetTxProcess(uid, grayllTx)
			if err != nil && err.Error() != "redis: nil" {
				log.Println("[ERROR] unable get txprocess infor", uid, err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Can not get tx infor")
				return
			}
			if prc == "BUSY" {
				msg := fmt.Sprintf("[HACK] transaction is busy %s - %s", uid, grayllTx)
				docRef := h.apiContext.Store.Collection("hacks/" + uid + "/closed").NewDoc()
				docRef.Set(ctx, map[string]interface{}{
					"msg":  msg,
					"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
				})
				log.Println(msg)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "")
				return
			}

			h.apiContext.Cache.SetTxProcess(uid, grayllTx)

			// Get position from database
			position, err := h.apiContext.Store.Doc("algo_positions/users/" + uid + "/" + grayllTx).Get(ctx)
			if err != nil {
				log.Printf("[ERROR] %s unable to get position with id : %s - err: %v", uid, grayllTx, err)
				h.apiContext.Store, _ = ReconnectFireStore(h.apiContext.ProjectID, 60)
				position, err = h.apiContext.Store.Doc("algo_positions/users/" + uid + "/" + grayllTx).Get(ctx)
				if err != nil {
					log.Printf("[ERROR] unable to get position with after retry : %s - err: %v", uid, grayllTx, err)
					GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
					return
				}
			}

			var param AlgorithmPosition
			err = MapToStruct(&param, position.Data())
			if err != nil {
				log.Printf("[ERROR] unable to unmarshal GRY open position request's body, error : %v", err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}
			if param.Status != "OPEN" {
				msg := fmt.Sprintf("[HACK] transaction is busy %s - %s", uid, grayllTx)
				docRef := h.apiContext.Store.Collection("hacks/" + uid + "/closed").NewDoc()
				docRef.Set(ctx, map[string]interface{}{
					"msg":  msg,
					"time": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
				})
				log.Println(msg)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "closed")
				return
			}
			closeAllAlgo.Data = append(closeAllAlgo.Data, param)
			//log.Println("params:", param)
		}

		inputUpdated := new(AlgoCloseAllInputData)
		inputUpdated.Action = "CLOSEALL"
		inputUpdated.Data = make([]AlgorithmPosition, 0)
		batch := h.apiContext.Store.Batch()

		for _, params := range closeAllAlgo.Data {
			if err != nil {
				log.Printf("[ERROR] unable to unmarshal GRY open position request's body, error : %v", err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}

			position, err := h.apiContext.Store.Doc("algo_positions/users/" + uid + "/" + params.GrayllTxId).Get(ctx)
			if err != nil {
				log.Printf("[ERROR] unable to get position with id : %s - err: %v", params.GrayllTxId, err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}

			err = MapToStruct(&params, position.Data())
			if err != nil {
				log.Printf("[ERROR] unable to unmarshal GRY open position request's body, error : %v", err)
				GinRespond(c, http.StatusOK, INVALID_PARAMS, "Close algo position invalid params")
				return
			}
			//log.Println("params:", params)

			params.CloseValueGRX = grxusd
			params.CloseValueGRZ = grzusd

			params.ClosePositionTotalUSD = params.OpenPositionValueUSD * (((grzusd - params.OpenValueGRZ) / params.OpenValueGRZ) + 1)
			params.ClosePositionFeeUSD = params.ClosePositionTotalUSD * 0.003
			params.ClosePositionFeeGRX = params.ClosePositionFeeUSD / grxusd

			params.ClosePositionROIUSD = params.ClosePositionTotalUSD - params.OpenPositionValueUSD
			params.ClosePerformanceFeeUSD = 0
			netRoi := params.ClosePositionROIUSD - params.ClosePositionFeeUSD
			// if netRoi <= 0 {
			// 	netRoi = 2
			// }
			if netRoi > 0 {
				refererUid, _ := h.apiContext.Cache.GetRefererUid(uid)
				fee := 0.18
				if refererUid != "" {
					fee = 0.15
				}

				params.ClosePerformanceFeeUSD = netRoi * fee
				params.ClosePerformanceFeeGRX = params.ClosePerformanceFeeUSD / grxusd
				if refererUid != "" {
					perfFeeReferer := netRoi * 0.03 / grxusd
					params.ClosePerfFeeRefererGRX, _ = strconv.ParseFloat(fmt.Sprintf("%.7f", perfFeeReferer), 64)
					params.RefererId = refererUid
				}
			}

			//     let close_position_total_GRX = close_position_total_$/grxusd
			params.ClosePositionTotalGRX = params.ClosePositionTotalUSD / grxusd
			//     let close_position_value_$ = close_position_total_$ - close_position_fee_$ - close_performance_fee_$
			params.ClosePositionValueUSD = params.ClosePositionTotalUSD - params.ClosePositionFeeUSD - params.ClosePerformanceFeeUSD
			params.ClosePositionValueGRX = params.ClosePositionValueUSD / grxusd
			//log.Println("ClosePositionValueGRX:", params.ClosePositionValueGRX)
			//     let close_position_ROI_percent = (grzusd - position.open_value_GRZ)*100/position.open_value_GRZ
			params.ClosePositionROIPercentage = (grzusd - params.OpenValueGRZ) * 100 / params.OpenValueGRZ
			//     let close_position_ROI_percent_NET = ((close_position_value_$-position.open_position_value_$)*100)/position.open_position_value_$
			params.Close_position_ROI_percent_NET = ((params.ClosePositionValueUSD - params.OpenPositionValueUSD) * 100) / params.OpenPositionValueUSD

			params.UserId = uid
			params.ClosePositionTimestamp = time.Now().Unix()
			params.Status = "CLOSED"
			params.Action = "CLOSED"
			params.DurationString = ConvertSectoDay(time.Now().Unix() - params.OpenPositionTimestamp)

			_params, err := json.Marshal(params)
			if err != nil {
				log.Printf("[ERROR] unable to marshal _grzPosition data, error : %v", err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Open algo position internal error")
				return
			}

			//log.Println("_params:", string(_params))

			positionMap := make(map[string]interface{})
			err = json.Unmarshal(_params, &positionMap)

			docPath := fmt.Sprintf("algo_positions/users/%s/%s", params.UserId, params.GrayllTxId)
			docRef := h.apiContext.Store.Doc(docPath)

			batch.Set(docRef, positionMap, firestore.MergeAll)
			inputUpdated.Data = append(inputUpdated.Data, params)
		}
		_, err = batch.Commit(ctx)
		if err != nil {
			log.Println("[ERROR] can not commit close all: ", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "can not update data")
			return
		}
		for _, grayllTx := range input.GrayllTxs {
			h.apiContext.Cache.DelTxProcess(uid, grayllTx)
		}

		body, _ := json.Marshal(&inputUpdated)

		err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, body)
		if err != nil {
			log.Println("[ERROR] closeallposition Can not publish message", uid, err)
			h.PubSubClient, _ = ReconnectPubsub(h.apiContext.ProjectID, 60)
			err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, body)
			if err != nil {
				log.Println("[ERROR] closeallposition can not publish message after retry", uid, err)
			}
		}

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS,
		})

	}
}

func (h UserHandler) UpdatePosition() gin.HandlerFunc {
	return func(c *gin.Context) {
		scheduleTime := time.Now().Unix() + 60
		ctx := context.Background()
		updateMap := make(map[string]interface{})
		err := c.BindJSON(&updateMap)

		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ update position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Update postion error")
			return
		}
		//log.Println("update req:", updateMap)
		// check current status of position

		uid := updateMap["user_id"].(string)
		grayllTxId := updateMap["grayll_transaction_id"].(string)
		openPositionValue := updateMap["open_position_value_$"].(float64)
		openValueGRZ := updateMap["open_value_GRZ"].(float64)
		openPositionTime := int64(updateMap["open_position_timestamp"].(float64))

		subQueueId := ""
		if val, ok := updateMap["sub_queue_id"]; ok {
			subQueueId = val.(string)
		}

		// Schedule next 1 minute
		// _, err = h.ScheduleTask(updateMap, scheduleTime, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
		// if err != nil {
		// 	log.Println("[ERROR] UpdatePosition ScheduleTask", uid, err)
		// }

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", uid, grayllTxId)
		positionDoc, err := h.apiContext.Store.Doc(docPath).Get(ctx)
		if err != nil {
			log.Printf("[ERROR] %s unable to get position with id : %s - err: %v", uid, grayllTxId, err)
			h.apiContext.Store, _ = ReconnectFireStore(h.apiContext.ProjectID, 60)
			positionDoc, err = h.apiContext.Store.Doc(docPath).Get(ctx)
			if err != nil {
				log.Printf("[ERROR] %s unable to get position with id : %s - err: %v", uid, grayllTxId, err)
				GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Update algo position internal error")
				return
			}

		}

		if positionDoc.Data()["status"].(string) == "CLOSED" {
			c.JSON(http.StatusOK, gin.H{
				"errCode": SUCCESS,
			})
			return
		}

		currentGrxusd, err := h.apiContext.Cache.GetGRXUsd()
		if err != nil {
			log.Println("[ERROR] unable to get grz from redis", uid, err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Get grxusd error")
		}
		currentGrzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			log.Println("[ERROR] unable to get grz from redis", uid, err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Get grzusd error")
		}

		positionMap := make(map[string]interface{})

		//let current_position_value_$ = position.open_position_value_$ * ((grzusd - position.open_value_GRZ)/position.open_value_GRZ)  + position.open_position_value_$
		currentValue := openPositionValue * (((currentGrzusd - openValueGRZ) / openValueGRZ) + 1)
		currentROI := currentValue - openPositionValue
		//currentValue := openPositionValue + currentROI
		currentValueGRX := currentValue / currentGrxusd
		currentROIPercent := (currentGrzusd - openValueGRZ) * 100 / openValueGRZ

		duration := time.Now().Unix() - openPositionTime
		// last update at used to check whether position task update stopped- client will check this.
		positionMap["last_update_at"] = time.Now().Unix()
		positionMap["user_id"] = uid
		positionMap["grayll_transaction_id"] = grayllTxId
		positionMap["duration"] = duration
		positionMap["duration_string"] = ConvertSectoDay(duration)
		positionMap["current_value_GRX"] = currentGrxusd
		positionMap["current_value_GRZ"] = currentGrzusd
		positionMap["current_position_value_$"] = currentValue
		positionMap["open_value_GRZ"] = openValueGRZ
		positionMap["current_position_value_GRX"] = currentValueGRX
		positionMap["current_position_ROI_$"] = currentROI
		positionMap["current_position_ROI_percent"] = currentROIPercent

		// Schedule next 1 minute
		_, err = h.ScheduleTask(updateMap, scheduleTime, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
		if err != nil {
			log.Println("[ERROR] UpdatePosition scheduleTask", uid, err)
			h.apiContext.CloudTaskClient, err = ReconnectCloudTask(h.apiContext.ProjectID, 60)
			_, err := h.ScheduleTask(updateMap, scheduleTime, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
			if err != nil {
				log.Println("[ERROR] update Reconnect cloud task client but failed", uid, err)
			}
		}

		_, err = h.apiContext.Store.Doc(docPath).Set(ctx, positionMap, firestore.MergeAll)
		if err != nil {
			log.Println("[ERROR] Save gryz algo position data error: ", uid, err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Update algo position internal error")
			return
		}

		//positionMap["status"] = "UPDATE"
		positionMap["action"] = "UPDATE"
		_positionMap, err := json.Marshal(&positionMap)
		if err != nil {
			log.Println("[ERROR] Marshal positionMap: ", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Update algo position internal error")
			return
		}

		// Publish for grayll-system
		err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopic, _positionMap)
		if err != nil {
			log.Println("[ERROR] closeallposition Can not publish message", uid, err)
			h.PubSubClient, _ = ReconnectPubsub(h.apiContext.ProjectID, 60)
			err = PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopicOpenClose, _positionMap)
			if err != nil {
				log.Println("[ERROR] closeallposition can not publish message after retry", uid, err)
			}
		}

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS,
		})
	}
}

func (h UserHandler) PerformanceUpdate() gin.HandlerFunc {
	return func(c *gin.Context) {
		scheduleTime := time.Now().Unix() + 24*60*60
		ctx := context.Background()
		updateMap := make(map[string]interface{})
		err := c.BindJSON(&updateMap)

		if err != nil {
			log.Printf("[ERROR] unable to unmarshal GRZ update position request's body, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Update postion error")
			return
		}
		//log.Println("PerformanceUpdate req:", updateMap)
		// check current status of position

		uid := updateMap["user_id"].(string)
		grayllTxId := updateMap["grayll_transaction_id"].(string)
		openPositionValue := updateMap["open_position_value_$"].(float64)
		openValueGRZ := updateMap["open_value_GRZ"].(float64)
		openPositionTime := int64(updateMap["open_position_timestamp"].(float64))
		last12hGRZValue := openValueGRZ
		if val, ok := updateMap["last_12h_value_GRZ"]; ok {
			last12hGRZValue = val.(float64)
		}
		//log.Println("PerformanceUpdate-last12hGRZValue", last12hGRZValue)

		docPath := fmt.Sprintf("algo_positions/users/%s/%s", uid, grayllTxId)
		positionDoc, err := h.apiContext.Store.Doc(docPath).Get(ctx)
		if err != nil {
			log.Println("[ERROR] get algo position data on grz-arkady: ", docPath, err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Update algo position internal error")
			return
		}

		if positionDoc.Data()["status"].(string) == "CLOSED" {
			c.JSON(http.StatusOK, gin.H{
				"errCode": SUCCESS,
			})
			return
		}

		// doc, err := h.apiContext.Store.Doc("price_update/794retePzavE19bTcMaH").Get(ctx)
		// if err != nil {
		// 	log.Printf("[ERROR] unable to get update_price document, error : %v", err)
		// 	GinRespond(c, http.StatusOK, INVALID_PARAMS, "Get update_price error")
		// 	return
		// }

		currentGrxusd, err := h.apiContext.Cache.GetGRXUsd()
		if err != nil {
			log.Printf("[ERROR] unable to get update_price document, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Get update_price error")
		}
		currentGrzusd, err := h.apiContext.Cache.GetGRZUsd()
		if err != nil {
			log.Printf("[ERROR] unable to get update_price document, error : %v", err)
			GinRespond(c, http.StatusOK, INVALID_PARAMS, "Get update_price error")
		}
		//log.Println("PerformanceUpdate-currentGrzusd", currentGrzusd)

		positionMap := make(map[string]interface{})

		currentROI := (currentGrzusd - openValueGRZ) * openPositionValue / openValueGRZ
		currentValue := openPositionValue + currentROI
		currentValueGRX := currentValue / currentGrxusd
		currentROIPercent := (currentGrzusd - openValueGRZ) * 100 / openValueGRZ
		duration := time.Now().Unix() - openPositionTime
		positionMap["user_id"] = uid
		positionMap["grayll_transaction_id"] = grayllTxId
		positionMap["duration"] = duration
		positionMap["algorithm_type"] = "GRZ"
		// if val, ok := updateMap["algorithm_type"]; ok {
		// 	positionMap["algorithm_type"] = val.(string)
		// }
		//positionMap["algorithm_type"] = updateMap["algorithm_type"].(string)

		positionMap["duration_string"] = ConvertSectoDay(duration)
		positionMap["current_value_GRX"] = currentGrxusd
		positionMap["current_value_GRZ"] = currentGrzusd
		positionMap["current_position_value_$"] = currentValue

		positionMap["current_position_value_GRX"] = currentValueGRX
		positionMap["current_position_ROI_$"] = currentROI
		positionMap["current_position_ROI_percent"] = currentROIPercent
		log.Println("PerformanceUpdate-last_12h_ROI_percent", (currentGrzusd-last12hGRZValue)*100/last12hGRZValue)
		positionMap["last_12h_ROI_percent"] = (currentGrzusd - last12hGRZValue) * 100 / last12hGRZValue

		positionMap["open_position_value_$"] = updateMap["open_position_value_$"]
		positionMap["open_value_GRZ"] = updateMap["open_value_GRZ"]
		positionMap["open_position_timestamp"] = updateMap["open_position_timestamp"]

		positionMap["status"] = "PERFORMANCE"
		positionMap["action"] = "PERFORMANCE"

		_positionMap, err := json.Marshal(&positionMap)
		if err != nil {
			log.Println("[ERROR] Marshal positionMap: ", err)
			GinRespond(c, http.StatusOK, INTERNAL_ERROR, "Update algo position internal error")
			return
		}

		// Publish for grayll-system
		PublishMessage(h.PubSubClient, h.apiContext.Config.PositionTopic, _positionMap)

		// Schedule next 12 hours
		updateMap["last_12h_value_GRZ"] = currentGrzusd
		h.ScheduleTask(updateMap, scheduleTime, h.apiContext.Config.PerformanceUrl, h.apiContext.Config.PerfQueueId)

		c.JSON(http.StatusOK, gin.H{
			"errCode": SUCCESS,
		})

	}
}

// type PubSubMessage struct {
// 	Data []byte `json:"data"`
// }
type PubSubMessage struct {
	Message struct {
		Data string `json:"data,omitempty"`
	} `json:"message"`
}
type PriceH struct {
	Price  float64 `json:"price"`
	UnixTs int64   `json:"unixts"`
	GRType string  `json:"grtype"`
	Frame  string  `json:"frame"`
}

func (h UserHandler) MvpPrice() gin.HandlerFunc {
	return func(c *gin.Context) {

		psMsg := PubSubMessage{}
		err := c.BindJSON(&psMsg)
		if err != nil {
			log.Println("MvpPrice-error bind json: ", err)
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		decoded, err := base64.StdEncoding.DecodeString(psMsg.Message.Data)
		if err != nil {
			fmt.Println("decode error:", err)
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}
		//log.Println("decoded:", string(decoded))
		priceh := PriceH{}
		err = json.Unmarshal(decoded, &priceh)
		if err != nil {
			//data, err := ioutil.ReadAll(c.Request.Body)
			log.Println("MvpPrice-error Unmarshal json: ", err)
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}
		//log.Printf("MvpPrice-gry price %s - time %s", price)
		if priceh.GRType == "gryusd" {
			h.apiContext.Cache.SetGRYUsd(priceh.Price)
		}
		if priceh.GRType == "grzusd" {
			h.apiContext.Cache.SetGRZUsd(priceh.Price)
		}

		batch := h.apiContext.Store.Batch()
		docPath := fmt.Sprintf("asset_algo_values/%s/%s", priceh.GRType, priceh.Frame)
		docRef := h.apiContext.Store.Collection(docPath).NewDoc()
		batch.Create(docRef, map[string]interface{}{
			price:          priceh.Price,
			UNIX_timestamp: priceh.UnixTs,
		})

		if priceh.Frame == "frame_01m" && (priceh.GRType == "gryusd" || priceh.GRType == "grzusd") {
			log.Println("PUBLISH PRICE: ", priceh.GRType, priceh.Frame, time.Unix(priceh.UnixTs, 0).Format("2006-01-02 15:04:05"))
			priceStr := priceh.GRType[:3] + "usd"
			docRefPrice := h.apiContext.Store.Doc("price_update/794retePzavE19bTcMaH")
			batch.Set(docRefPrice, map[string]interface{}{
				"price_updated": priceStr,
				priceStr:        priceh.Price,
			}, firestore.MergeAll)
		}
		_, err = batch.Commit(context.Background())
		if err != nil {
			log.Println("Batch commit error: ", err)
			c.Status(http.StatusOK)
			return
		}

		c.Status(http.StatusOK)

	}
}

func PublishMessage(pubsubClient *pubsub.Client, topicID string, msg []byte) error {
	ctx := context.Background()
	t := pubsubClient.Topic(topicID)
	result := t.Publish(ctx, &pubsub.Message{Data: msg})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	_, err := result.Get(ctx)
	if err != nil {
		log.Printf("Published error: %v\n", err)
		return fmt.Errorf("Get: %v", err)
	}
	//log.Printf("Published message ID: %v\n", id)
	return nil
}

func structToMap(st AlgorithmPosition) map[string]interface{} {
	m := make(map[string]interface{})
	elem := reflect.ValueOf(&st).Elem()
	relType := elem.Type()
	for i := 0; i < relType.NumField(); i++ {
		m[relType.Field(i).Name] = elem.Field(i).Interface()
	}
	return m
}

// createHTTPTask creates a new task with a HTTP target then adds it to a Queue.
func (h UserHandler) createUpdateTask(message []byte, execTime int64, taskURL, queueId string) (*taskspb.Task, error) {

	// Create a new Cloud Tasks client instance.
	// See https://godoc.org/cloud.google.com/go/cloudtasks/apiv2
	ctx := context.Background()
	// opt := option.WithCredentialsFile("grayll-grz-arkady-bda9949575fc.json")
	// client, err := cloudtasks.NewClient(ctx, opt)
	// if err != nil {
	// 	return nil, fmt.Errorf("NewClient: %v", err)
	// }

	// Build the Task queue path.
	queuePath := fmt.Sprintf("projects/%s/locations/%s/queues/%s", h.apiContext.Config.ProjectId, h.apiContext.Config.LocationId, queueId)
	ts := new(timestamp.Timestamp)
	ts.Seconds = execTime

	// Build the Task payload.
	// https://godoc.org/google.golang.org/genproto/googleapis/cloud/tasks/v2#CreateTaskRequest
	req := &taskspb.CreateTaskRequest{
		Parent: queuePath,
		Task: &taskspb.Task{
			// https://godoc.org/google.golang.org/genproto/googleapis/cloud/tasks/v2#HttpRequest
			MessageType: &taskspb.Task_HttpRequest{
				HttpRequest: &taskspb.HttpRequest{
					HttpMethod: taskspb.HttpMethod_POST,
					Url:        taskURL,
					AuthorizationHeader: &taskspb.HttpRequest_OidcToken{
						OidcToken: &taskspb.OidcToken{
							ServiceAccountEmail: h.apiContext.Config.ServiceAccountEmail,
						},
					},
				},
			},
			ScheduleTime: ts,
		},
	}

	// Add a payload message if one is present.
	req.Task.GetHttpRequest().Body = message

	createdTask, err := h.apiContext.CloudTaskClient.CreateTask(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("cloudtasks.CreateTask: %v", err)
	}

	return createdTask, nil
}

// UpdatePositionTask runs every minute to update the
func (h UserHandler) ScheduleTask(data map[string]interface{}, scheduleTime int64, taskURL, queueId string) (*taskspb.Task, error) {
	json, _ := json.Marshal(data)
	task, err := h.createUpdateTask(json, scheduleTime, taskURL, queueId)
	if err != nil {
		log.Println("createHTTPTask error:", err)
	}
	// else {
	// 		log.Println("createHTTPTask schedule time:", task.GetScheduleTime())
	// 		log.Println("createHTTPTask task name:", task.GetName())
	// 	}
	return task, err

}
func (h UserHandler) ReScheduleAlgoPosition() gin.HandlerFunc {
	return func(c *gin.Context) {

		ctx := context.Background()

		iter := h.apiContext.Store.Collection("algo_positions").Doc("users").Collections(ctx)

		for {
			collRef, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				//return err
			}
			//fmt.Printf("Found collection with id: %v\n", collRef.ID)
			iter1 := h.apiContext.Store.Collection("algo_positions/users/"+collRef.ID).Where("status", "==", "OPEN").Documents(ctx)
			for {
				docref, err := iter1.Next()
				if err == iterator.Done {
					break
				}
				//fmt.Printf("Found doc with id: %v\n", docref.Data())
				if _updateAt, ok := docref.Data()["last_update_at"]; ok {
					updateAt := int64(0)
					switch _updateAt.(type) {
					case int64:
						log.Println("is int64")
						updateAt = docref.Data()["last_update_at"].(int64)
					case float64:
						log.Println("is float64")
						updateAt = int64(docref.Data()["last_update_at"].(float64))
					}
					// reschedule task is not updated with 2 hours
					if updateAt < time.Now().Unix()-2*60*60 {
						openTime := int64(docref.Data()["open_position_timestamp"].(float64))
						scheduleTime := openTime + ((time.Now().Unix()-openTime)/60+2)*60
						timestr := time.Unix(scheduleTime, 0).Format("2006-01-02 15:04:05")
						fmt.Printf("This needs to reschedule at %s - id: %v\n", timestr, docref.Ref.ID)
						data := map[string]interface{}{
							"user_id":                 docref.Data()["user_id"].(string),
							"grayll_transaction_id":   docref.Data()["grayll_transaction_id"].(string),
							"open_position_value_$":   docref.Data()["open_position_value_$"].(float64),
							"open_value_GRZ":          docref.Data()["open_value_GRZ"].(float64),
							"open_position_timestamp": openTime,
							"algorithm_type":          docref.Data()["algorithm_type"].(string),
						}
						// This will run every one minute
						h.ScheduleTask(data, scheduleTime, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId)
					}

				}

			}
		}
	}

}
func (h UserHandler) ReScheduleTask() {

	ctx := context.Background()

	iter := h.apiContext.Store.Collection("algo_positions").Doc("users").Collections(ctx)
	for {
		collRef, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			//return err
		}
		//fmt.Printf("Found collection with id: %v\n", collRef.ID)
		iter1 := h.apiContext.Store.Collection("algo_positions/users/"+collRef.ID).Where("status", "==", "OPEN").Documents(ctx)
		for {
			docref, err := iter1.Next()
			if err == iterator.Done {
				break
			}
			//fmt.Printf("Found doc with id: %v\n", docref.Data())
			if _updateAt, ok := docref.Data()["last_update_at"]; ok {
				updateAt := int64(0)
				switch _updateAt.(type) {
				case int64:
					log.Println("is int64")
					updateAt = docref.Data()["last_update_at"].(int64)
				case float64:
					log.Println("is float64")
					updateAt = int64(docref.Data()["last_update_at"].(float64))
				}
				// reschedule task is not updated with 2 hours
				//if updateAt < time.Now().Unix()-60 {
				if updateAt < time.Now().Unix()-2*60*60 {
					openTime := int64(docref.Data()["open_position_timestamp"].(float64))
					scheduleTime := openTime + ((time.Now().Unix()-openTime)/60+2)*60
					timestr := time.Unix(scheduleTime, 0).Format("2006-01-02 15:04:05")
					fmt.Printf("This needs to reschedule at %s - id: %v\n", timestr, docref.Ref.ID)

					grayllTxId := docref.Data()["grayll_transaction_id"].(string)
					grayllTxIdNumber, _ := strconv.Atoi(strings.TrimLeft(grayllTxId, "0"))
					subQueueId := "-" + strconv.Itoa(grayllTxIdNumber%h.QueueNumber)

					data := map[string]interface{}{
						"user_id":                 docref.Data()["user_id"].(string),
						"grayll_transaction_id":   docref.Data()["grayll_transaction_id"].(string),
						"open_position_value_$":   docref.Data()["open_position_value_$"].(float64),
						"open_value_GRZ":          docref.Data()["open_value_GRZ"].(float64),
						"open_position_timestamp": openTime,
						"algorithm_type":          docref.Data()["algorithm_type"].(string),
						"sub_queue_id":            subQueueId,
					}

					fmt.Printf("This needs to reschedule at %s - id: %v - queue: %s\n", timestr, docref.Ref.ID, h.apiContext.Config.QueueId+subQueueId)
					// This will run every one minute
					h.ScheduleTask(data, scheduleTime, h.apiContext.Config.TaskUrl, h.apiContext.Config.QueueId+subQueueId)
				}

			}

		}
	}

}

func (h UserHandler) Warmup() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	}
}

func ReconnectFireStore(projectId string, timeout int) (*firestore.Client, error) {
	cnt := 0
	var client *firestore.Client
	var err error
	ctx := context.Background()
	for {
		cnt++
		time.Sleep(1 * time.Second)
		// conf := &firebase.Config{ProjectID: projectId}

		// app, err := firebase.NewApp(ctx, conf)
		// if err != nil {
		// 	return nil, err
		// }
		// client, err = app.Firestore(ctx)
		client, err = firestore.NewClient(ctx, projectId)

		if err == nil {
			break
		}
		if cnt > timeout {
			log.Println("[ERROR] Can not connect to firestore after retry times", cnt, projectId, err)
			break
		}

	}
	return client, err
}

func ReconnectCloudTask(projectId string, timeout int) (*cloudtasks.Client, error) {
	cnt := 0
	var client *cloudtasks.Client
	var err error
	ctx := context.Background()
	for {
		cnt++
		time.Sleep(1 * time.Second)
		client, err = cloudtasks.NewClient(ctx)

		if err == nil {
			break
		}
		if cnt > timeout {
			log.Println("[ERROR] Can not create cloud task client after ", cnt, projectId, err)
			break
		}

	}
	return client, err
}

func ReconnectPubsub(projectId string, timeout int) (*pubsub.Client, error) {

	var err error
	var pubsubClient *pubsub.Client
	ctx := context.Background()
	cnt := 0
	for {
		cnt++
		time.Sleep(1 * time.Second)
		pubsubClient, err = pubsub.NewClient(ctx, "grayll-system")
		if err != nil {
			log.Println("pubsub.NewClient error:", err)
			return nil, err
		}

		if err == nil {
			break
		}
		if cnt > timeout {
			log.Println("[ERROR] Can not connect pubsub client after ", cnt, projectId, err)
			break
		}
	}
	return pubsubClient, err
}
