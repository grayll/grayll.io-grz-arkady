package api

import (
	"encoding/json"

	webpush "github.com/SherClockHolmes/webpush-go"
)

//{"publicKey":"BGHhiED8J7t9KwJlEgNXT-EDIJQ1RZPorhuSYtufaRezRTGhofadZtrgZ8MVa0pwISEyBZRaYa-Bzl9MHtwaF9s",
//"privateKey":"i-WIZXB71GM-uuLi30sG1-JANzKKF_HpgUpz5Rt8aos"}
func PushNotice(data map[string]interface{}, sub *webpush.Subscription) error {
	decoded, err := json.Marshal(data)
	_, err = webpush.SendNotification(decoded, sub, &webpush.Options{
		Subscriber:      "info@grayll.io",
		VAPIDPrivateKey: "i-WIZXB71GM-uuLi30sG1-JANzKKF_HpgUpz5Rt8aos",
		VAPIDPublicKey:  "BGHhiED8J7t9KwJlEgNXT-EDIJQ1RZPorhuSYtufaRezRTGhofadZtrgZ8MVa0pwISEyBZRaYa-Bzl9MHtwaF9s",
		TTL:             30,
	})
	return err
}
