## Fix issue: build bitbucket.org/grayll/grayll.io-user-app-back-end: cannot load gopkg.in/russross/blackfriday.v2: cannot find module providing package gopkg.in/russross/blackfriday.v2

=> go mod edit -replace=gopkg.in/russross/blackfriday.v2@v2.0.1=github.com/russross/blackfriday/v2@v2.0.1

## Build and deploy
gcloud config set project grayll-grz-arkady && 
gcloud builds submit --tag gcr.io/grayll-grz-arkady/grayll-grz-arkady &&
gcloud beta run deploy --image gcr.io/grayll-grz-arkady/grayll-grz-arkady --platform managed --set-env-vars SERVER=prod,RE_SCHEDULE=1 SUPER_ADMIN_ADDRESS=,SUPER_ADMIN_SEED=,SELLING_PRICE=,SELLING_PERCENT=

gcloud run services update [SERVICE] --set-env-vars KEY1=VALUE1,KEY2=VALUE2

gcloud config set project grayll-grz-arkady && 
gcloud builds submit --tag gcr.io/grayll-grz-arkady/grayll-grz-arkady-dev &&
gcloud beta run deploy --image gcr.io/grayll-grz-arkady/grayll-grz-arkady-dev --platform managed --set-env-vars SERVER=dev,RE_SCHEDULE=0

## Federation project
gcloud config set project grayll-federation
gcloud functions deploy Query --runtime go111 --trigger-http

## Cloud task
he time between retries will double max_doublings times.

A task's retry interval starts at min_backoff, then doubles max_doublings times, then increases linearly, and finally retries retries at intervals of max_backoff up to max_attempts times.

For example, if min_backoff is 10s, max_backoff is 300s, and max_doublings is 3, then the a task will first be retried in 10s. The retry interval will double three times, and then increase linearly by 2^3 * 10s. Finally, the task will retry at intervals of max_backoff until the task has been attempted max_attempts times. Thus, the requests will retry at 10s, 20s, 40s, 80s, 160s, 240s, 300s, 300s, ....

If unspecified when the queue is created, Cloud Tasks will pick the default.

This field is output only for pull queues.

https://cloud.google.com/tasks/docs/configuring-queues
A task will be scheduled for retry between minBackoff and maxBackoff duration after it fails, if the queue's RetryConfig specifies that the task should be retried.
gcloud tasks queues describe grz-update
name: projects/grayll-grz-arkady/locations/us-central1/queues/grz-update
purgeTime: '2020-06-19T17:33:04.742853Z'
rateLimits:
  maxBurstSize: 100
  maxConcurrentDispatches: 5000
  maxDispatchesPerSecond: 500.0
retryConfig:
  maxAttempts: 7200
  maxBackoff: 60s
  maxDoublings: 1
  maxRetryDuration: 432000s
  minBackoff: 30s
state: RUNNING



gcloud config set project grayll-grz-arkady && 
gcloud tasks queues update gry-update --max-attempts=30 --min-backoff=120s --max-backoff=480s --max-doublings=1 
120 - 240 - 480 - 480+4*120=960 - 1200 - 1200 - 1200 - 1200 - 1200 - 1200

gcloud config set project grayll-grz-arkady && 
gcloud tasks queues update grz-update --max-attempts=30 --min-backoff=240s --max-backoff=480s --max-doublings=1 --max-retry-duration=43800s

gcloud tasks queues create grz-performance --max-attempts=7200 --min-backoff=300s --max-backoff=3600s --max-doublings=16 --max-retry-duration=432000s --max-dispatches-per-second=500 --max-concurrent-dispatches=5000

gcloud config set project grayll-grz-arkady && 
gcloud tasks queues create retry-failed-algo --max-attempts=10 --min-backoff=30s --max-backoff=60s --max-doublings=1 --max-retry-duration=600s --max-dispatches-per-second=500 --max-concurrent-dispatches=5000

## Set cloud run env
--set-env-vars
--update-env-vars
--remove-env-vars
--clear-env-vars

## Register cloud run with pubsub

1-Enable Pub/Sub to create authentication tokens in your project:
gcloud projects add-iam-policy-binding grayll-grz-arkady \
     --member=serviceAccount:service-665265048111@gcp-sa-pubsub.iam.gserviceaccount.com \
     --role=roles/iam.serviceAccountTokenCreator

2- Create or select a service account to represent the Pub/Sub subscription identity.
gcloud iam service-accounts create cloud-run-pubsub-invoker \
     --display-name "Cloud Run Pub/Sub Invoker"

3- Create a Pub/Sub subscription with the service account using the appropriate tab depending on whether you are using Cloud Run or Cloud Run for Anthos on Google Cloud.
a - Give the invoker service account permission to invoke your pubsub-tutorial service:
gcloud run services add-iam-policy-binding grayll-grz-arkady \
   --member=serviceAccount:cloud-run-pubsub-invoker@grayll-grz-arkady.iam.gserviceaccount.com \
   --role=roles/run.invoker

b - Create a Pub/Sub subscription with the service account:
gcloud pubsub subscriptions create mvp-price-gryz --topic mvp_price \
   --push-endpoint=https://grayll-grz-arkady-itovmvefzq-uc.a.run.app/api/v1/mvpprice/ \
   --push-auth-service-account=cloud-run-pubsub-invoker@grayll-grz-arkady.iam.gserviceaccount.com

Replace

myRunTopic with the topic you previously created.
SERVICE-URL with the HTTPS URL provided on deploying the service. This URL works even if you have also added a domain mapping.
PROJECT_ID with your Google Cloud project ID.
The --push-auth-service-account flag activates the Pub/Sub push functionality for Authentication and authorization.

Your Cloud Run service domain is automatically registered for use with Pub/Sub subscriptions.

For Cloud Run only, there is a built-in authentication check that the token is valid and an authorization check that the service account has permission to invoke the Cloud Run service.


