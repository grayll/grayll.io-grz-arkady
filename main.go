package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"time"

	"cloud.google.com/go/pubsub"

	"bitbucket.org/grayll/grayll.io-grz-arkady/api"
	jwttool "bitbucket.org/grayll/grayll.io-grz-arkady/jwt-tool"

	"github.com/gin-gonic/gin"
	"github.com/huyntsgs/cors"

	_ "net/http/pprof"

	cloudtasks "cloud.google.com/go/cloudtasks/apiv2"
	"cloud.google.com/go/firestore"

	//utils "bitbucket.org/grayll/grayll.io-utils"

	stellar "github.com/huyntsgs/stellar-service"
	"github.com/huyntsgs/stellar-service/assets"
	"google.golang.org/api/option"

	libredis "github.com/go-redis/redis/v7"
	limiter "github.com/ulule/limiter/v3"
	mgin "github.com/ulule/limiter/v3/drivers/middleware/gin"
	sredis "github.com/ulule/limiter/v3/drivers/store/redis"
)

func main() {
	jwt, err := jwttool.NewJwtFromRsaKey("key/key.pem")
	if err != nil {
		log.Fatal("Error loading rsa key")
	}
	var store *firestore.Client
	var pubsubClient *pubsub.Client
	var cloudTaskClient *cloudtasks.Client

	ctx := context.Background()

	var config *api.Config
	srv := os.Getenv("SERVER")
	if srv == "prod" {
		config = parseConfig("config1.json")
		// } else if srv == "dev" {
		// 	config = parseConfig("config1-dev.json")
	} else {
		config = parseConfig("config.json")
	}

	// if prod == "1" {
	// 	config = parseConfig("config1.json")
	// } else {
	// 	config = parseConfig("config.json")
	// }
	asset := assets.Asset{Code: config.AssetCode, IssuerAddress: config.IssuerAddress}
	//log.Println(config.HotWalletOne)
	//spew.Dump(config)
	var liquidityClient *firestore.Client
	if config.IsMainNet || srv != "" {
		config.IsMainNet = true
		store, err = GetFsClient(false, "grayll-grz-arkady", "")
		if err != nil {
			log.Fatalln("main: GetFsClient error: ", err)
		}
		liquidityClient, err = GetFsClient(false, "grayll-liquidity", "")
		pubsubClient, err = pubsub.NewClient(ctx, "grayll-system")
		if err != nil {
			log.Println("pubsub.NewClient error:", err)
			return
		}
		cloudTaskClient, err = cloudtasks.NewClient(ctx)
	} else {
		config.IsMainNet = false
		store, err = GetFsClient(true, "grayll-grz-arkady", "grayll-grz-arkady-firebase-adminsdk-9q3s2-3198dd40fa.json")
		if err != nil {
			log.Fatalln("main: GetFsClient error: ", err)
		}
		liquidityClient, err = GetFsClient(true, "grayll-liquidity", "grayll-liquidity-firebase-adminsdk-yl4ki-38da970995.json")
		if err != nil {
			log.Fatalln("main: GetFsClient error: ", err)
		}
		opt := option.WithCredentialsFile("./grayll-system-b2317e64062b.json")
		pubsubClient, err = pubsub.NewClient(ctx, "grayll-system", opt)
		if err != nil {
			log.Println("pubsub.NewClient error:", err)
			return
		}
		opt1 := option.WithCredentialsFile("./grayll-grz-arkady-528f3c71b2da.json")
		cloudTaskClient, err = cloudtasks.NewClient(ctx, opt1)
	}

	log.Println("Mainnet:", config.IsMainNet, "Horizonurl:", config.HorizonUrl)
	stellar.SetupParam(float64(1000), config.IsMainNet, config.HorizonUrl)
	ttl, _ := time.ParseDuration("12h")
	cache := api.NewRedisCache(ttl, config)
	// Profiler initialization, best done as early as possible.
	// go func() {
	// 	if err := profiler.Start(profiler.Config{
	// 		Service:        "grz-akardy-server",
	// 		ServiceVersion: "1.0.0",
	// 		ProjectID:      "grayll-grz-arkady",
	// 	}); err != nil {
	// 		log.Println("Profiler error", err)
	// 	}
	// }()

	appContext := &api.ApiContext{Store: store, Jwt: jwt, Cache: cache, Config: config, Asset: asset, CloudTaskClient: cloudTaskClient}
	appContext.LiquidityClient = liquidityClient
	appContext.ProjectID = "grayll-grz-arkady"

	router := SetupRouter(appContext, pubsubClient, srv)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8088"
	}
	router.Run(":" + port)
}

func SetupRouter(appContext *api.ApiContext, pubsubClient *pubsub.Client, srv string) *gin.Engine {
	rateFormat := "13-M"
	rl := os.Getenv("RATE_LIMIT")
	if rl != "" {
		rateFormat = rl
	}
	rate, err := limiter.NewRateFromFormatted(rateFormat)
	if err != nil {
		log.Fatal(err)
	}
	client := libredis.NewClient(&libredis.Options{
		Addr:     fmt.Sprintf("%s:%d", appContext.Config.RedisHost, appContext.Config.RedisPort),
		Password: appContext.Config.RedisPass})

	// Create a store with the redis client.
	store, err := sredis.NewStoreWithOptions(client, limiter.StoreOptions{
		Prefix:   "gry1_limit",
		MaxRetry: 2,
	})
	if err != nil {
		log.Fatal(err)
	}
	// Create a new middleware with the limiter instance.
	middleware := mgin.NewMiddleware(limiter.New(store, rate))

	router := gin.New()

	// router.Use(cors.New(cors.Config{
	// 	AllowOrigins: []string{"https://app.grayll.io", "http://127.0.0.1:4200"},
	// 	//AllowOrigins:     []string{"https://app.grayll.io", "http://127.0.0.1:8081", "http://127.0.0.1:4200"},
	// 	AllowMethods:     []string{"POST, GET, OPTIONS, PUT, DELETE"},
	// 	AllowHeaders:     []string{"Authorization", "Origin", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token"},
	// 	ExposeHeaders:    []string{"Content-Length"},
	// 	AllowCredentials: true,
	// 	MaxAge:           24 * time.Hour,
	// }))
	allowedKeyWords := []string{"performanceupdate", "updateposition", "retryfailalgo", "mvpprice"}
	if srv == "prod" || srv == "dev" {
		router.Use(cors.New(cors.Config{
			AllowOrigins:     []string{"https://app.grayll.io", "http://127.0.0.1:4200"},
			AllowMethods:     []string{"POST, GET, OPTIONS, PUT, DELETE"},
			AllowHeaders:     []string{"Authorization", "Origin", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token"},
			ExposeHeaders:    []string{"Content-Length"},
			AllowCredentials: true,
			AllowURLKeywords: allowedKeyWords,
			MaxAge:           24 * time.Hour,
		}))
	} else {
		router.Use(cors.New(cors.Config{
			AllowOrigins:     []string{"http://127.0.0.1:4200"},
			AllowMethods:     []string{"POST, GET, OPTIONS, PUT, DELETE"},
			AllowHeaders:     []string{"Authorization", "Origin", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token"},
			ExposeHeaders:    []string{"Content-Length"},
			AllowCredentials: true,
			AllowURLKeywords: allowedKeyWords,
			MaxAge:           24 * time.Hour,
		}))
	}
	//router.Use(cors.Default())
	router.Use(gin.Recovery())

	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s | %s | %s | %d | %s  | %s | %s | %s\n",
			param.TimeStamp.Format(time.RFC3339),
			param.Keys["Uid"],
			param.ClientIP,
			param.StatusCode,
			param.Latency,
			param.Method,
			param.Path,
			param.ErrorMessage,
		)
	}))

	//productHandler := api.NewProductHandle(store)
	queueNumber := 50
	queueNumberEnv := os.Getenv("QUEUE_NUMBER")
	if queueNumberEnv != "" {
		queueNumber, _ = strconv.Atoi(queueNumberEnv)
	}

	userHandler := api.NewUserHandler(appContext)
	userHandler.QueueNumber = queueNumber
	userHandler.PubSubClient = pubsubClient
	reschedule := os.Getenv("RE_SCHEDULE")

	if reschedule == "1" {
		go userHandler.ReScheduleTask()
	}

	//phones := api.NewPhoneHandler(appContext)

	// Always has versioning for api
	// Default(initial) is v1
	v1 := router.Group("/api/v1")
	v1.POST("/updateposition", userHandler.UpdatePosition())
	v1.POST("/performanceupdate", userHandler.PerformanceUpdate())
	v1.GET("/warmup", userHandler.Warmup())
	v1.POST("/mvpprice", userHandler.MvpPrice())
	v1.POST("/retryfailalgo", userHandler.ReTryFailAlgo())

	// apis needs to be authenticated
	v1.Use(middleware).Use(api.Authorize(appContext.Jwt))
	{
		v1.POST("/grz/position/open", userHandler.OpenPostion())
		v1.POST("/grz/position/close", userHandler.ClosePosition())
		v1.POST("/grz/position/closeAll", userHandler.CloseAllPosition())

	}
	return router
}
