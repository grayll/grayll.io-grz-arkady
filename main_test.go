package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"testing"

	"context"
	"log"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"

	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

const (
	// gray price
	UNIX_timestamp = "UNIX_timestamp"

//price          = "price"
)

func TestVerify(t *testing.T) {
	QueryAlgoPosition()
	//UpdateFailedAlgoPosition()
	//UpdateFailedAlgoPositionAll()
}
func QueryAlgoPosition() {

	ctx := context.Background()
	opt := option.WithCredentialsFile("./grayll-app-f3f3f3-firebase-adminsdk-vhclm-e074da6170.json")
	// app, err := firebase.NewApp(ctx, nil, opt)
	// if err != nil {
	// 	log.Fatalln("Error create new firebase app:", err)
	// }

	// client, err := app.Firestore(ctx)
	client, err := firestore.NewClient(ctx, "grayll-app-f3f3f3", opt)

	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	iter := client.Collection("algo_positions").Doc("users").Collections(ctx)
	for {
		collRef, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			//return err
		}
		//fmt.Printf("Found collection with id: %v\n", collRef.ID)
		iter1 := client.Collection("algo_positions/users/"+collRef.ID).Where("status", "==", "OPEN").Documents(ctx)
		for {
			docref, err := iter1.Next()
			if err == iterator.Done {
				break
			}
			//fmt.Printf("Found doc with id: %v\n", docref.Data())
			if _, ok := docref.Data()["last_update_at"]; ok {
				updateAt := int64(docref.Data()["last_update_at"].(float64))
				// reschedule task is not updated with 2 hours
				if updateAt < time.Now().Unix()-20*60 {
					openTime := int64(docref.Data()["open_position_timestamp"].(float64))

					scheduleTime := openTime + ((time.Now().Unix()-openTime)/60+2)*60
					timestr := time.Unix(scheduleTime, 0).Format("2006-01-02 15:04:05")
					lastUpdateAt := time.Unix(updateAt, 0).Format("2006-01-02 15:04:05")
					algorithm := docref.Data()["algorithm_type"].(string)
					if algorithm == "GRY 1" {
						grayllTxId := docref.Data()["grayll_transaction_id"].(string)

						grayllTxIdNumber, _ := strconv.Atoi(strings.TrimLeft(grayllTxId, "0"))
						subQueueId := "-" + strconv.Itoa(grayllTxIdNumber%100)
						log.Println("grayllTxIdNumber:", grayllTxIdNumber, subQueueId)

					}

					fmt.Printf("This needs to reschedule at %s . Last update at: %s, - %s - %s - id: %v\n", timestr, lastUpdateAt, docref.Data()["user_id"].(string), algorithm, docref.Ref.ID)
				}

			}

		}

	}
}
func UpdateFailedAlgoPositionAll() {

	ctx := context.Background()
	opt1 := option.WithCredentialsFile("./grayll-grz-arkady-firebase-adminsdk-9q3s2-3198dd40fa.json")
	appgrz, err := firebase.NewApp(ctx, nil, opt1)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	clientGrz, err := appgrz.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	opt := option.WithCredentialsFile("./grayll-app-f3f3f3-firebase-adminsdk-vhclm-e074da6170.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	iter := client.Collection("algo_positions").Doc("users").Collections(ctx)
	for {
		collRef, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			//return err
		}
		//fmt.Printf("Found collection with id: %v\n", collRef.ID)
		iter1 := client.Collection("algo_positions/users/"+collRef.ID).Where("status", "==", "OPEN").Documents(ctx)
		for {
			doc, err := iter1.Next()
			if err == iterator.Done {
				break
			}
			if _, ok := doc.Data()["open_position_value_$"]; !ok {
				log.Println(doc.Data()["grayll_transaction_id"].(string), doc.Data()["user_id"].(string), doc.Data()["algorithm_type"].(string))

				doc1, _ := clientGrz.Doc("algo_positions/users/" + collRef.ID + "/" + doc.Data()["grayll_transaction_id"].(string)).Get(ctx)
				log.Println(doc1.Data()["open_position_value_$"].(float64), doc1.Data()["user_id"].(string))

				//batch.Set(doc.Ref, doc1.Data(), firestore.MergeAll)

			}

		}

	}
}
func UpdateFailedAlgoPosition() {

	ctx := context.Background()
	opt := option.WithCredentialsFile("./grayll-app-f3f3f3-firebase-adminsdk-vhclm-e074da6170.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	opt1 := option.WithCredentialsFile("./grayll-grz-arkady-firebase-adminsdk-9q3s2-3198dd40fa.json")
	appgrz, err := firebase.NewApp(ctx, nil, opt1)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	clientGrz, err := appgrz.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	docPath := "algo_positions/users/umaNCgumfzTPKqWp_wWCGfwLveNrNK29bhfqHHGTgSo"
	docs, _ := client.Collection(docPath).Documents(ctx).GetAll()
	batch := client.Batch()

	for _, doc := range docs {
		if _, ok := doc.Data()["open_position_value_$"]; !ok {
			log.Println(doc.Data()["grayll_transaction_id"].(string), doc.Data()["user_id"].(string))

			doc1, _ := clientGrz.Doc(docPath + "/" + doc.Data()["grayll_transaction_id"].(string)).Get(ctx)
			log.Println(doc1.Data()["open_position_value_$"].(float64), doc1.Data()["user_id"].(string))

			batch.Set(doc.Ref, doc1.Data(), firestore.MergeAll)

		}
	}
	_, err = batch.Commit(ctx)
	if err != nil {
		log.Println(err)
	}
	// for {
	// 	collRef, err := iter.Next()
	// 	if err == iterator.Done {
	// 		break
	// 	}
	// 	if err != nil {
	// 		//return err
	// 	}
	// 	//fmt.Pif _, ok := docref.Data()["open_position_value_$"]; !ok {
	// 			log.Println(docref.Data()["grayll_transaction_id"].(string), log.Println(docref.Data()["user_id"].(string)))
	// 		}rintf("Found collection with id: %v\n", collRef.ID)
	// 	iter1 := client.Collection("algo_positions/users/"+collRef.ID).Where("status", "==", "OPEN").Documents(ctx)
	// 	for {
	// 		docref, err := iter1.Next()
	// 		if err == iterator.Done {
	// 			break
	// 		}
	// 		if _, ok := docref.Data()["open_position_value_$"]; !ok {
	// 			log.Println(docref.Data()["grayll_transaction_id"].(string), log.Println(docref.Data()["user_id"].(string)))
	// 		}
	// 		//fmt.Printf("Found doc with id: %v\n", docref.Data())
	// 		// if _, ok := docref.Data()["last_update_at"]; ok {
	// 		// 	updateAt := int64(docref.Data()["last_update_at"].(float64))
	// 		// 	// reschedule task is not updated with 2 hours
	// 		// 	if updateAt < time.Now().Unix()-2*60*60 {
	// 		// 		openTime := int64(docref.Data()["open_position_timestamp"].(float64))

	// 		// 		scheduleTime := openTime + ((time.Now().Unix()-openTime)/60+2)*60
	// 		// 		timestr := time.Unix(scheduleTime, 0).Format("2006-01-02 15:04:05")
	// 		// 		lastUpdateAt := time.Unix(updateAt, 0).Format("2006-01-02 15:04:05")
	// 		// 		algorithm := docref.Data()["algorithm_type"].(string)
	// 		// 		fmt.Printf("This needs to reschedule at %s . Last update at: %s, - %s - %s - id: %v\n", timestr, lastUpdateAt, docref.Data()["user_id"].(string), algorithm, docref.Ref.ID)

	// 		// 	}

	// 		// }

	// 	}

	// }
}

func QueryGrz() {
	//var startTs int64 = 1569949920 + int64(1440*60)
	//var startTs int64 = 1563469200
	var startTs int64 = 1572519421

	ctx := context.Background()
	opt := option.WithCredentialsFile("../grayll-mvp-firebase-adminsdk-jhq9x-cd9d4774ad.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	//fsclient := FireStoreClient{client}
	var curPrice float64
	var curTime int64
	//var pair string
	docPath := "grz_price_frames/grzusd/frame_30m"
	it := client.Collection(docPath).Where(UNIX_timestamp, ">", startTs).OrderBy(UNIX_timestamp, firestore.Asc).Limit(10).Documents(context.Background())
	for {
		doc, err := it.Next()
		if err == iterator.Done {
			return
		}

		if err != nil {
			log.Println("Error get latest price: ", err)
			return
		}

		curTime = doc.Data()[UNIX_timestamp].(int64)
		//pair = doc.Data()["pair"].(string)
		curPrice = doc.Data()["price"].(float64)

		fmt.Printf("time: %d - price: %f \n", curTime, curPrice)
		priceTime := time.Unix(curTime, 0).Format("2006-01-02 15:04:05")
		fmt.Println("priceTime:", priceTime)

		// Add missing data for next day
		// client.Collection("pair_frames/gryusd/frame_01d").Add(context.Background(), map[string]interface{}{
		// 	UNIX_timestamp: curTime,
		// 	price:          curPrice,
		// })

	}

}

func DelGrz() {
	//var startTs int64 = 1569949920 + int64(1440*60)
	//var startTs int64 = 1563469200

	var startTs int64 = 1572519421

	ctx := context.Background()
	opt := option.WithCredentialsFile("../grayll-mvp-firebase-adminsdk-jhq9x-cd9d4774ad.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	//fsclient := FireStoreClient{client}
	var curPrice float64
	var curTime int64
	//var pair string
	docPaths := []string{"grz_price_frames/grzusd/frame_01m", "grz_price_frames/grzusd/frame_05m", "grz_price_frames/grzusd/frame_15m", "grz_price_frames/grzusd/frame_30m", "grz_price_frames/grzusd/frame_01h",
		"grz_price_frames/grzusd/frame_04h", "grz_price_frames/grzusd/frame_01d"}

	for _, docPath := range docPaths {
		it := client.Collection(docPath).Where(UNIX_timestamp, "<", startTs).OrderBy(UNIX_timestamp, firestore.Desc).Limit(500).Documents(context.Background())
		for {
			doc, err := it.Next()
			if err == iterator.Done {
				return
			}

			if err != nil {
				log.Println("Error get latest price: ", err)
				return
			}

			curTime = doc.Data()[UNIX_timestamp].(int64)
			//pair = doc.Data()["pair"].(string)
			curPrice = doc.Data()["price"].(float64)

			fmt.Printf("time: %d - price: %f \n", curTime, curPrice)
			priceTime := time.Unix(curTime, 0).Format("2006-01-02 15:04:05")
			fmt.Println("priceTime:", priceTime, doc.Ref.ID)

			//client.Collection(docPath).Doc(doc.Ref.ID).Delete(ctx)

			// Add missing data for next day
			// client.Collection("pair_frames/gryusd/frame_01d").Add(context.Background(), map[string]interface{}{
			// 	UNIX_timestamp: curTime,
			// 	price:          curPrice,
			// })

		}
	}

}

func QueryOriginalGrz() {
	//var startTs int64 = 1569949920 + int64(1440*60)
	//var startTs int64 = 1563469200

	var startTs int64 = 1573636800 - 2*60*60

	ctx := context.Background()
	opt := option.WithCredentialsFile("../grayll-mvp-firebase-adminsdk-jhq9x-cd9d4774ad.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	//fsclient := FireStoreClient{client}
	var curPrice float64
	var curTime int64
	//var pair string
	docPath := "grz_price"
	it := client.Collection(docPath).Where("UNIX_timestamp", ">=", startTs).OrderBy(UNIX_timestamp, firestore.Desc).Documents(context.Background())
	for {
		doc, err := it.Next()
		if err == iterator.Done {
			return
		}

		if err != nil {
			log.Println("Error get latest price: ", err)
			return
		}

		curTime = doc.Data()[UNIX_timestamp].(int64)
		//pair = doc.Data()["pair"].(string)
		curPrice = doc.Data()["price"].(float64)

		priceTime := time.Unix(curTime, 0).Format("2006-01-02 15:04:05")
		fmt.Printf("time: %d (%s) - price: %f \n", curTime, priceTime, curPrice)

		// Add missing data for next day
		// client.Collection("pair_frames/gryusd/frame_01d").Add(context.Background(), map[string]interface{}{
		// 	UNIX_timestamp: curTime,
		// 	price:          curPrice,
		// })

	}

}

func QueryOriginalGry() {
	//var startTs int64 = 1569949920 + int64(1440*60)
	//var startTs int64 = 1563469200

	//	var startTs int64 = 1575684300

	ctx := context.Background()
	opt := option.WithCredentialsFile("./grayll-app-f3f3f3-firebase-adminsdk-vhclm-e074da6170.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatalln("Error create new firebase app:", err)
	}

	//fsclient := FireStoreClient{client}
	var curPrice float64
	var curTime int64
	//var pair string
	docPath := "asset_algo_values/gryusd/frame_01m"
	it := client.Collection(docPath).OrderBy("UNIX_timestamp", firestore.Desc).Limit(20).Documents(context.Background())
	for {
		doc, err := it.Next()
		if err == iterator.Done {
			return
		}

		if err != nil {
			log.Println("Error get latest price: ", err)
			return
		}

		curTime = doc.Data()[UNIX_timestamp].(int64)
		//pair = doc.Data()["pair"].(string)
		curPrice = doc.Data()["price"].(float64)

		priceTime := time.Unix(curTime, 0).Format("2006-01-02 15:04:05")
		fmt.Printf("time: %d (%s) - price: %f \n", curTime, priceTime, curPrice)
		//fmt.Println("priceTime:", priceTime)

		// Add missing data for next day
		// client.Collection("pair_frames/gryusd/frame_01d").Add(context.Background(), map[string]interface{}{
		// 	UNIX_timestamp: curTime,
		// 	price:          curPrice,
		// })

	}

}

// func TestQueryDB(t *testing.T) {
// 	var startTs int64 = 1556989920 - 5

// 	ctx := context.Background()
// 	opt := option.WithCredentialsFile("./grayll-mvp-firebase-adminsdk-jhq9x-cd9d4774ad.json")
// 	app, err := firebase.NewApp(ctx, nil, opt)
// 	if err != nil {
// 		log.Fatalln("Error create new firebase app:", err)
// 	}

// 	client, err := app.Firestore(ctx)
// 	if err != nil {
// 		log.Fatalln("Error create new firebase app:", err)
// 	}

// 	//fsclient := FireStoreClient{client}
// 	var curPrice float64
// 	var curTime int64
// 	var pair string
// 	it := client.Collection("medianclose").Where(UNIX_timestamp, ">=", startTs).OrderBy(UNIX_timestamp, firestore.Asc).Limit(8).Documents(context.Background())
// 	for {
// 		doc, err := it.Next()
// 		if err == iterator.Done {
// 			return
// 		}

// 		if err != nil {
// 			log.Println("Error get latest price: ", err)
// 			return
// 		}

// 		curTime = doc.Data()[UNIX_timestamp].(int64)
// 		pair = doc.Data()["pair"].(string)
// 		curPrice = doc.Data()[medain_close].(float64)

// 		fmt.Printf("coin: %s - time: %d - price: %f \n", pair, curTime, curPrice)
// 		priceTime := time.Unix(curTime, 0).Format("2006-01-02 15:04:05")
// 		fmt.Println("priceTime:", priceTime)

// 	}

// }
